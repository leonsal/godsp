package winfunc

//go:generate goxtempl -in=func.t -out=gen_f32.go  TName=F32  TType=float32
//go:generate goxtempl -in=func.t -out=gen_f64.go  TName=F64  TType=float64
//go:generate goxtempl -in=func.t -out=gen_c64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=func.t -out=gen_c128.go TName=C128 TType=complex128
