package winfunc

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("WINFUNC", logger.Default())
