package winfunc

import (
	"math"
)

// Wintype specifies the window function type
type Wintype int

const (
	// Rectangular specifies a rectangular window function.
	Rectangular = Wintype(iota)
	// Triangular specifies a rectangular window function.
	Triangular
	// Hann specifies a Hann window function.
	Hann
	// Hamming specifies a Hamming window function.
	Hamming
	// Blackman specifies a Blackman window function.
	Blackman
	// BlackmanHarris specifies a Blackman-Harris window function.
	BlackmanHarris
	// FlatTop specifies a Flat Top window function.
	FlatTop
)

// TriangularFunc calculates the value of the triangular window function
// for the i element of a vector of n elements.
func TriangularFunc(n, i int) float64 {

	return 1 - math.Abs((float64(i)-float64(n-1)/2)/(float64(n)/2))
}

// HannFunc calculates the value of the Hann window function
// for the i element of a vector of n elements.
func HannFunc(n, i int) float64 {

	return 0.5 - 0.5*math.Cos((2*math.Pi*float64(i))/float64(n))
}

// HammingFunc calculates the value of the Hamming window function
// for the i element of a vector of n elements.
func HammingFunc(n, i int) float64 {

	return 0.54 - 0.46*math.Cos((2*math.Pi*float64(i))/float64(n))
}

// BlackmanFunc calculates the value of the Blackman window function
// for the i element of a vector of n elements
func BlackmanFunc(n, i int) float64 {

	const a0 = 0.42
	const a1 = 0.5
	const a2 = 0.08
	return a0 -
		a1*math.Cos((2*math.Pi*float64(i))/float64(n)) +
		a2*math.Cos((4*math.Pi*float64(i))/float64(n))
}

// BlackmanHarrisFunc calculates the value of the Blackman-Harris window function
// for the i element of a vector of n elements
func BlackmanHarrisFunc(n, i int) float64 {

	const a0 = 0.35875
	const a1 = 0.48829
	const a2 = 0.14128
	const a3 = 0.01168
	return a0 -
		a1*math.Cos((2*math.Pi*float64(i))/float64(n)) +
		a2*math.Cos((4*math.Pi*float64(i))/float64(n)) +
		a3*math.Cos((6*math.Pi*float64(i))/float64(n))
}

// FlatTopFunc calculates the value of the Flat top window function
// for the i element of a vector of n elements
func FlatTopFunc(n, i int) float64 {

	const a0 = 1
	const a1 = 1.93
	const a2 = 1.29
	const a3 = 0.388
	const a4 = 0.028
	return a0 -
		a1*math.Cos(2*math.Pi*float64(i)/float64(n)) +
		a2*math.Cos(4*math.Pi*float64(i)/float64(n)) -
		a3*math.Cos(6*math.Pi*float64(i)/float64(n)) +
		a4*math.Cos(8*math.Pi*float64(i)/float64(n))
}
