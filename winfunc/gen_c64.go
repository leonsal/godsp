package winfunc

import (
	"github.com/leonsal/godsp/core"
)

// VC64 implements a node which applies an window function to
// input vectors of type complex64. The initial window function is Rectangular
// which is equivalent to no window function.
type VC64 struct {
	core.Node                   // embedded node
	inpData   *core.InputVC64   // data input
	Type      *core.InputUINT   // window function type input
	outs      []*core.InputVC64 // inputs connected to the output
	bufs      [][]complex64     // output buffers
	bufi      int               // current output buffer index
	wftype    Wintype           // current window function type
	wflen     int               // current length of window function input vector
	mult      []complex64       // function multipliers
}

// NewVC64 creates and returns a pointer to a new window function node
// for input vectors of type complex64.
func NewVC64(wftype Wintype) *VC64 {

	w := new(VC64)
	w.inpData = core.NewInputVC64(w)
	w.Type = core.NewInputUINT(w)
	w.Type.Set(uint(wftype))
	w.wftype = wftype
	w.wflen = 0
	return w
}

// Input returns the input with the specified name
// Returns nil if not found
func (w *VC64) Input(name string) interface{} {

	switch name {
	case "data":
		return w.inpData
	default:
		return nil
	}
}

// Connect connects this window function output to the specified input
func (w *VC64) Connect(inp interface{}) {

	w.outs = append(w.outs, inp.(*core.InputVC64))
}

// Run reads the input data and the window function type and
// then applies the window function to the input data and sends
// the result to connected inputs.
func (w *VC64) Run() {

	// Read data input
	data := w.inpData.Read()
	fsize := len(data)

	// Reads window function type input and recalculates window function
	// multipliers if necessary
	wftype := w.Type.Read()
	if wftype != uint(w.wftype) || w.wflen != fsize {
		w.wftype = Wintype(wftype)
		w.wflen = fsize
		w.recalc()
	}

	// Fast path for Rectangular function
	if w.wftype == Rectangular {
		// Sends original input to connected inputs
		for i := 0; i < len(w.outs); i++ {
			w.outs[i].Write(data)
		}
		return
	}

	// Reallocates output buffers if necessary
	w.bufs = core.ReallocBuffersVC64(w.bufs, core.OutBuffers, fsize)

	// Apply the window function
	buf := w.bufs[w.bufi]
	for i := 0; i < fsize; i++ {
		buf[i] = data[i] * w.mult[i]
	}

	// Sends output to connected inputs
	for i := 0; i < len(w.outs); i++ {
		w.outs[i].Write(buf[:fsize])
	}

	// Prepare to use next output buffer
	w.bufi++
	if w.bufi >= len(w.bufs) {
		w.bufi = 0
	}
}

// recalc recalculates the window function multipliers for
// the current window type and length
func (w *VC64) recalc() {

	// Reallocates multipliers buffer if necessary
	n := w.wflen
	if n > len(w.mult) {
		w.mult = make([]complex64, n)
	}

	if w.wftype == Rectangular {
		return
	}

	var wfunc func(n, i int) float64
	switch w.wftype {
	case Triangular:
		wfunc = TriangularFunc
	case Hann:
		wfunc = HannFunc
	case Hamming:
		wfunc = HammingFunc
	case Blackman:
		wfunc = BlackmanFunc
	case BlackmanHarris:
		wfunc = BlackmanHarrisFunc
	case FlatTop:
		wfunc = FlatTopFunc
	default:
		panic("Invalid window function type")
	}

	for i := 0; i < n; i++ {
		w.mult[i] = complex64(complex(wfunc(n, i), 0))
	}
}
