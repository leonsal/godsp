package util

import (
	"github.com/leonsal/godsp/core"
)

// ChanVI16 allows for intercepting the communication of two nodes
// by reading an internal channel with the received input.
type ChanVI16 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVI16   // Input data
	outs      []*core.InputVI16 // array of connected inputs
	c         chan []int16      // channel
}

// NewChanVI16 creates and returns a pointer to ChanVI16
func NewChanVI16(clen int) *ChanVI16 {

	t := new(ChanVI16)
	t.inpData = core.NewInputVI16(t)
	t.c = make(chan []int16, clen)
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *ChanVI16) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *ChanVI16) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVI16))
}

// Read reads this node channel. It may block
func (t *ChanVI16) Read() []int16 {

	return <-t.c
}

// Peek try to read this node channel.
// Returns nil if the channel is empty
func (t *ChanVI16) Peek() []int16 {

	select {
	case v := <-t.c:
		return v
	default:
		return nil
	}
}

// Run reads the input, sends to the connected outputs and
// also to the internal channel.
func (t *ChanVI16) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends to channel (may block)
	select {
	case t.c <- data:
	default:
	}
}
