package util

import (
	"fmt"
	"github.com/leonsal/godsp/core"
)

// DumperVF32 is node which dumps the input data
type DumperVF32 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF32   // Data input
	maxlen    int               // maximum number of elements to dump
	cols      int               // number of columns
	format    string            // format of each element
	outs      []*core.InputVF32 // array of connected inputs
}

// NewDumperVF32 creates and returns a pointer to DumperVF32
func NewDumperVF32(maxlen, cols int, format string) *DumperVF32 {

	d := new(DumperVF32)
	d.inpData = core.NewInputVF32(d)
	d.maxlen = maxlen
	d.cols = cols
	d.format = format
	return d
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (d *DumperVF32) Input(name string) interface{} {

	switch name {
	case "data":
		return d.inpData
	default:
		return nil
	}
	return nil
}

// Connect this source output to the specified input
func (d *DumperVF32) Connect(inp interface{}) {

	d.outs = append(d.outs, inp.(*core.InputVF32))
}

// Run runs this node once.
func (d *DumperVF32) Run() {

	// Read input
	data := d.inpData.Read()

	// Dumps data
	col := 1
	for i := 0; i < len(data); i++ {
		if i >= d.maxlen {
			break
		}
		v := fmt.Sprintf(d.format, data[i])
		fmt.Printf("%s", v)
		col++
		if col > d.cols {
			col = 1
			fmt.Printf("\n")
		}
	}
	fmt.Println()

	// Sends to connected inputs
	for i := 0; i < len(d.outs); i++ {
		d.outs[i].Write(data)
	}
}
