package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVI16 allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVI16 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVI16   // Input data
	outs      []*core.InputVI16 // array of connected inputs
	cb        func([]int16)     // function callback
}

// NewTapVI16 creates and returns a pointer to node tap for int16
func NewTapVI16() *TapVI16 {

	t := new(TapVI16)
	t.inpData = core.NewInputVI16(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVI16) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVI16) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVI16))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVI16) SetCallback(cb func([]int16)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVI16) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
