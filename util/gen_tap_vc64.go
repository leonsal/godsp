package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVC64 allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVC64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVC64   // Input data
	outs      []*core.InputVC64 // array of connected inputs
	cb        func([]complex64) // function callback
}

// NewTapVC64 creates and returns a pointer to node tap for complex64
func NewTapVC64() *TapVC64 {

	t := new(TapVC64)
	t.inpData = core.NewInputVC64(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVC64) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVC64) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVC64))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVC64) SetCallback(cb func([]complex64)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVC64) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
