package util

//go:generate goxtempl -in=dumper.t -out=gen_dumper_i16.go  TName=I16  TType=int16
//go:generate goxtempl -in=dumper.t -out=gen_dumper_f32.go  TName=F32  TType=float32
//go:generate goxtempl -in=dumper.t -out=gen_dumper_f64.go  TName=F64  TType=float64
//go:generate goxtempl -in=dumper.t -out=gen_dumper_c64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=dumper.t -out=gen_dumper_c128.go TName=C128 TType=complex128

//go:generate goxtempl -in=tap.t -out=gen_tap_vi16.go  TName=I16  TType=int16
//go:generate goxtempl -in=tap.t -out=gen_tap_vint.go  TName=INT  TType=int
//go:generate goxtempl -in=tap.t -out=gen_tap_vf32.go  TName=F32  TType=float32
//go:generate goxtempl -in=tap.t -out=gen_tap_vf64.go  TName=F64  TType=float64
//go:generate goxtempl -in=tap.t -out=gen_tap_vc64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=tap.t -out=gen_tap_vc128.go TName=C128 TType=complex128

//go:generate goxtempl -in=chan.t -out=gen_chan_vi16.go  TName=I16  TType=int16
//go:generate goxtempl -in=chan.t -out=gen_chan_vf32.go  TName=F32  TType=float32
//go:generate goxtempl -in=chan.t -out=gen_chan_vf64.go  TName=F64  TType=float64
