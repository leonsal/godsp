package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVF64 allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVF64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF64   // Input data
	outs      []*core.InputVF64 // array of connected inputs
	cb        func([]float64)   // function callback
}

// NewTapVF64 creates and returns a pointer to node tap for float64
func NewTapVF64() *TapVF64 {

	t := new(TapVF64)
	t.inpData = core.NewInputVF64(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVF64) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVF64) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVF64))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVF64) SetCallback(cb func([]float64)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVF64) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
