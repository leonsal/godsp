package util

import (
	"github.com/leonsal/godsp/core"
)

{{range .}}
// ChanV{{.TName}} allows for intercepting the communication of two nodes
// by reading an internal channel with the received input.
type ChanV{{.TName}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TName}}      // Input data
	outs    []*core.InputV{{.TName}}    // array of connected inputs
	c		chan []{{.TType}}			// channel
}

// NewChanV{{.TName}} creates and returns a pointer to ChanV{{.TName}}
func NewChanV{{.TName}}(clen int) *ChanV{{.TName}} {

	t := new(ChanV{{.TName}})
	t.inpData = core.NewInputV{{.TName}}(t)
	t.c = make(chan []{{.TType}}, clen)
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *ChanV{{.TName}}) Input(name string) interface{} {

    switch name {
    case "data":
        return t.inpData
    default:
        return nil
    }
    return nil
}

// Connect this tap output to the specified input
func (t *ChanV{{.TName}}) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputV{{.TName}}))
}

// Read reads this node channel. It may block
func (t *ChanV{{.TName}}) Read() []{{.TType}} {

    return <- t.c
}

// Peek try to read this node channel. 
// Returns nil if the channel is empty
func (t *ChanV{{.TName}}) Peek() []{{.TType}} {

    select {
        case v := <- t.c:
            return v
        default:
            return nil
    }
}

// Run reads the input, sends to the connected outputs and
// also to the internal channel.
func (t *ChanV{{.TName}}) Run() {

    // Read input
	data := t.inpData.Read()

    // Sends to connected inputs
    for i := 0; i < len(t.outs); i++ {
        t.outs[i].Write(data)
    }

    // Sends to channel (may block)
    select {
	    case t.c <- data:
        default:
    }
}

{{end}}
