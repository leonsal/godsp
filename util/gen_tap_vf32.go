package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVF32 allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVF32 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF32   // Input data
	outs      []*core.InputVF32 // array of connected inputs
	cb        func([]float32)   // function callback
}

// NewTapVF32 creates and returns a pointer to node tap for float32
func NewTapVF32() *TapVF32 {

	t := new(TapVF32)
	t.inpData = core.NewInputVF32(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVF32) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVF32) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVF32))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVF32) SetCallback(cb func([]float32)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVF32) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
