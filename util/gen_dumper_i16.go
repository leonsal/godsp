package util

import (
	"fmt"
	"github.com/leonsal/godsp/core"
)

// DumperVI16 is node which dumps the input data
type DumperVI16 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVI16   // Data input
	maxlen    int               // maximum number of elements to dump
	cols      int               // number of columns
	format    string            // format of each element
	outs      []*core.InputVI16 // array of connected inputs
}

// NewDumperVI16 creates and returns a pointer to DumperVI16
func NewDumperVI16(maxlen, cols int, format string) *DumperVI16 {

	d := new(DumperVI16)
	d.inpData = core.NewInputVI16(d)
	d.maxlen = maxlen
	d.cols = cols
	d.format = format
	return d
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (d *DumperVI16) Input(name string) interface{} {

	switch name {
	case "data":
		return d.inpData
	default:
		return nil
	}
	return nil
}

// Connect this source output to the specified input
func (d *DumperVI16) Connect(inp interface{}) {

	d.outs = append(d.outs, inp.(*core.InputVI16))
}

// Run runs this node once.
func (d *DumperVI16) Run() {

	// Read input
	data := d.inpData.Read()

	// Dumps data
	col := 1
	for i := 0; i < len(data); i++ {
		if i >= d.maxlen {
			break
		}
		v := fmt.Sprintf(d.format, data[i])
		fmt.Printf("%s", v)
		col++
		if col > d.cols {
			col = 1
			fmt.Printf("\n")
		}
	}
	fmt.Println()

	// Sends to connected inputs
	for i := 0; i < len(d.outs); i++ {
		d.outs[i].Write(data)
	}
}
