package util

import (
	"github.com/leonsal/godsp/core"
)

// ChanVF64 allows for intercepting the communication of two nodes
// by reading an internal channel with the received input.
type ChanVF64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF64   // Input data
	outs      []*core.InputVF64 // array of connected inputs
	c         chan []float64    // channel
}

// NewChanVF64 creates and returns a pointer to ChanVF64
func NewChanVF64(clen int) *ChanVF64 {

	t := new(ChanVF64)
	t.inpData = core.NewInputVF64(t)
	t.c = make(chan []float64, clen)
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *ChanVF64) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *ChanVF64) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVF64))
}

// Read reads this node channel. It may block
func (t *ChanVF64) Read() []float64 {

	return <-t.c
}

// Peek try to read this node channel.
// Returns nil if the channel is empty
func (t *ChanVF64) Peek() []float64 {

	select {
	case v := <-t.c:
		return v
	default:
		return nil
	}
}

// Run reads the input, sends to the connected outputs and
// also to the internal channel.
func (t *ChanVF64) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends to channel (may block)
	select {
	case t.c <- data:
	default:
	}
}
