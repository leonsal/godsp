package util

import (
	"github.com/leonsal/godsp/core"
)

{{range .}}
// TapV{{.TName}} allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapV{{.TName}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TName}}      // Input data
	outs    []*core.InputV{{.TName}}    // array of connected inputs
    cb      func([]{{.TType}})          // function callback
}

// NewTapV{{.TName}} creates and returns a pointer to node tap for {{.TType}}
func NewTapV{{.TName}}() *TapV{{.TName}} {

	t := new(TapV{{.TName}})
	t.inpData = core.NewInputV{{.TName}}(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapV{{.TName}}) Input(name string) interface{} {

    switch name {
    case "data":
        return t.inpData
    default:
        return nil
    }
    return nil
}

// Connect this tap output to the specified input
func (t *TapV{{.TName}}) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputV{{.TName}}))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapV{{.TName}}) SetCallback(cb func([]{{.TType}})) {
    
    t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapV{{.TName}}) Run() {

    // Read input
	data := t.inpData.Read()

    // Sends to connected inputs
    for i := 0; i < len(t.outs); i++ {
        t.outs[i].Write(data)
    }

    // Sends output to optional callback
    if t.cb != nil {
        t.cb(data)
    }
}
{{end}}

