package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVINT allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVINT struct {
	core.Node                   // Embedded node
	inpData   *core.InputVINT   // Input data
	outs      []*core.InputVINT // array of connected inputs
	cb        func([]int)       // function callback
}

// NewTapVINT creates and returns a pointer to node tap for int
func NewTapVINT() *TapVINT {

	t := new(TapVINT)
	t.inpData = core.NewInputVINT(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVINT) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVINT) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVINT))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVINT) SetCallback(cb func([]int)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVINT) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
