package util

import (
    "fmt"
	"github.com/leonsal/godsp/core"
)

{{range .}}
// DumperV{{.TName}} is node which dumps the input data
type DumperV{{.TName}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TName}}      // Data input
    maxlen  int                         // maximum number of elements to dump
    cols    int                         // number of columns
    format  string                      // format of each element
	outs    []*core.InputV{{.TName}}    // array of connected inputs
}

// NewDumperV{{.TName}} creates and returns a pointer to DumperV{{.TName}}
func NewDumperV{{.TName}}(maxlen, cols int, format string) *DumperV{{.TName}} {

	d := new(DumperV{{.TName}})
	d.inpData = core.NewInputV{{.TName}}(d)
	d.maxlen = maxlen
    d.cols = cols
    d.format = format
	return d
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (d *DumperV{{.TName}}) Input(name string) interface{} {

    switch name {
    case "data":
        return d.inpData
    default:
        return nil
    }
    return nil
}

// Connect this source output to the specified input
func (d *DumperV{{.TName}}) Connect(inp interface{}) {

	d.outs = append(d.outs, inp.(*core.InputV{{.TName}}))
}

// Run runs this node once.
func (d *DumperV{{.TName}}) Run() {

    // Read input
	data := d.inpData.Read()
    
    // Dumps data
    col := 1
    for i := 0; i < len(data); i++ {
        if i >= d.maxlen {
            break
        }
        v := fmt.Sprintf(d.format, data[i])
        fmt.Printf("%s", v)
        col++
        if col > d.cols {
            col = 1
            fmt.Printf("\n")
        }
    }  
    fmt.Println()

    // Sends to connected inputs
    for i := 0; i < len(d.outs); i++ {
        d.outs[i].Write(data)
    }
}
{{end}}

