package util

import (
	"github.com/leonsal/godsp/core"
)

// TapVC128 allows for intercepting the communication of two nodes
// calling an optional callback with the received input.
type TapVC128 struct {
	core.Node                    // Embedded node
	inpData   *core.InputVC128   // Input data
	outs      []*core.InputVC128 // array of connected inputs
	cb        func([]complex128) // function callback
}

// NewTapVC128 creates and returns a pointer to node tap for complex128
func NewTapVC128() *TapVC128 {

	t := new(TapVC128)
	t.inpData = core.NewInputVC128(t)
	t.cb = nil
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *TapVC128) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *TapVC128) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVC128))
}

// SetCallback sets the callback which will be called when the node
// receives an input.
func (t *TapVC128) SetCallback(cb func([]complex128)) {

	t.cb = cb
}

// Run reads the input and sends to the connected outputs and
// also to the optional callback
func (t *TapVC128) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends output to optional callback
	if t.cb != nil {
		t.cb(data)
	}
}
