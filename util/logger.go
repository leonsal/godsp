package util

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("UTIL", logger.Default())
