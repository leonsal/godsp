package util

import (
	"github.com/leonsal/godsp/core"
)

// ChanVF32 allows for intercepting the communication of two nodes
// by reading an internal channel with the received input.
type ChanVF32 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF32   // Input data
	outs      []*core.InputVF32 // array of connected inputs
	c         chan []float32    // channel
}

// NewChanVF32 creates and returns a pointer to ChanVF32
func NewChanVF32(clen int) *ChanVF32 {

	t := new(ChanVF32)
	t.inpData = core.NewInputVF32(t)
	t.c = make(chan []float32, clen)
	return t
}

// Input returns the input with the specified name
// Returns nil if not found
func (t *ChanVF32) Input(name string) interface{} {

	switch name {
	case "data":
		return t.inpData
	default:
		return nil
	}
	return nil
}

// Connect this tap output to the specified input
func (t *ChanVF32) Connect(inp interface{}) {

	t.outs = append(t.outs, inp.(*core.InputVF32))
}

// Read reads this node channel. It may block
func (t *ChanVF32) Read() []float32 {

	return <-t.c
}

// Peek try to read this node channel.
// Returns nil if the channel is empty
func (t *ChanVF32) Peek() []float32 {

	select {
	case v := <-t.c:
		return v
	default:
		return nil
	}
}

// Run reads the input, sends to the connected outputs and
// also to the internal channel.
func (t *ChanVF32) Run() {

	// Read input
	data := t.inpData.Read()

	// Sends to connected inputs
	for i := 0; i < len(t.outs); i++ {
		t.outs[i].Write(data)
	}

	// Sends to channel (may block)
	select {
	case t.c <- data:
	default:
	}
}
