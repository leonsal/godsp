package fourier

// genPermutationIndex builds the bit-inverted index vector,
// which is needed to permutate the input data.
func genPermutationIndex(P int) []int {

	N := 1 << uint(P)
	index := make([]int, N)
	index[0] = 0 // Initial sequence for N=1
	n := 1
	// For every next power of two, the
	// sequence is multiplied by 2 inplace.
	// Then the result is also appended to the
	// end and increased by one.
	for p := 0; p < P; p++ {
		for i := 0; i < n; i++ {
			index[i] <<= 1
			index[i+n] = index[i] + 1
		}
		n <<= 1
	}
	return index
}

// log2 returns the log base 2 of v
func log2(v uint) int {

	var r int
	for v >>= 1; v != 0; v >>= 1 {
		r++
	}
	return r
}
