package fourier

import (
	"fmt"
	"math"
)

// FFTRadix2VC64 implements the FFT transform using the radix2 algorithm
type FFTRadix2VC64 struct {
	n     int         // FFT length, power of 2.
	p     int         // Base-2 exponent: 2^p = N.
	roots []complex64 // Precomputed roots table, length N.
	perm  []int       // Index permutation vector for the input array.
}

// NewFFTRadix2VC64 creates and returns a pointer to a new FFT transform for
// inputs with the specified size and complex64 types
func NewFFTRadix2VC64(n int) (*FFTRadix2VC64, error) {

	if n&(n-1) != 0 {
		return nil, fmt.Errorf("FFT size must be power of 2")
	}
	f := &FFTRadix2VC64{n: n, p: log2(uint(n))}
	f.genRoots()
	f.perm = genPermutationIndex(f.p)
	return f, nil
}

// Forward calculates the forward FFT of the input data saving to the output buffer
func (f *FFTRadix2VC64) Forward(in, out []complex64) {

	if len(in) != f.n {
		panic("FFT input with wrong size.")
	}

	// Reorder the input array to the output
	for i := 0; i < f.n; i++ {
		out[i] = in[f.perm[i]]
	}

	// Do the butterfly with index i and j.
	butterfly := func(k, o, l, s int) {
		i := k + o
		j := i + l
		out[i], out[j] = out[i]+f.roots[k*s]*out[j], out[i]+f.roots[s*(k+l)]*out[j]
	}

	n := 1
	s := f.n // Stride
	for p := 1; p <= f.p; p++ {
		n <<= 1
		s >>= 1
		B := f.n / n // Number of blocks.
		for b := 0; b < B; b++ {
			K := f.n / (2 * B) // Half block length.
			o := 2 * b * K     // Block offset.
			for k := 0; k < K; k++ {
				butterfly(k, o, K, s)
			}
		}
	}
}

// Inverse is the backwards transform.
func (f *FFTRadix2VC64) Inverse(in, out []complex64) {

	if len(in) != f.n {
		panic("FFT input with wrong size.")
	}

	// Reverse the input vector
	for i := 1; i < f.n/2; i++ {
		j := f.n - i
		out[i], out[j] = in[j], in[i]
	}

	// Do the transform.
	f.Forward(in, out)

	// Scale the output by 1/N
	invN := 1.0 / float64(f.n)
	for i := 0; i < len(out); i++ {
		out[i] *= complex64(complex(invN, 0))
	}
}

// genRoots computes the complex-roots-of 1 table of length N.
func (f *FFTRadix2VC64) genRoots() {

	f.roots = make([]complex64, f.n)
	for i := 0; i < f.n; i++ {
		phi := -2.0 * math.Pi * float64(i) / float64(f.n)
		s, c := math.Sincos(phi)
		f.roots[i] = complex64(complex(c, s))
	}
}
