package fourier

import (
	"github.com/leonsal/godsp/core"
)

// FFTVC64 implements the FFT transform for vectors of complex64
type FFTVC64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVC64   // Data input
	outs      []*core.InputVC64 // array connected inputs
	bufs      [][]complex64     // output buffers
	bufi      int               // current output buffer index
	forward   bool              // transform direction
	size      int               // current size of the FFT
	fft       *FFTRadix2VC64    // FFT algorithm implementation
}

// NewFFTVC64 creates and returns a pointer to a new FFTVC64 with the
// specified direction
func NewFFTVC64(forward bool) *FFTVC64 {

	f := new(FFTVC64)
	f.forward = forward
	f.inpData = core.NewInputVC64(f)
	return f
}

// Input returns the input with the specified name
// Returns nil if not found
func (f *FFTVC64) Input(name string) interface{} {

	switch name {
	case "data":
		return f.inpData
	default:
		return nil
	}
	return nil
}

// Connect connects the specified input to this FFT output
func (f *FFTVC64) Connect(inp interface{}) {

	f.outs = append(f.outs, inp.(*core.InputVC64))
}

// Run reads input data, generates output buffer with the FFT
// of the input and send the result to connected inputs.
func (f *FFTVC64) Run() {

	// Reads input data
	data := f.inpData.Read()

	// Initialize FFT transform
	size := len(data)
	if size != f.size || f.fft == nil {
		fft, err := NewFFTRadix2VC64(size)
		if err != nil {
			panic(err)
		}
		f.fft = fft
		f.size = size
		log.Debug("FFTC64 initialized for size:%v", size)
	}

	// Reallocates output buffers if necessary
	f.bufs = core.ReallocBuffersVC64(f.bufs, core.OutBuffers, size)

	// Generates FFT
	buf := f.bufs[f.bufi]
	if f.forward {
		f.fft.Forward(data, buf)
	} else {
		f.fft.Inverse(data, buf)
	}

	// Sends to connected inputs
	for i := 0; i < len(f.outs); i++ {
		f.outs[i].Write(buf[:size])
	}

	// Prepare to use next output buffer
	f.bufi++
	if f.bufi >= len(f.bufs) {
		f.bufi = 0
	}
}
