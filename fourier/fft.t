package fourier

import (
	"github.com/leonsal/godsp/core"
)

{{range .}}
// FFTV{{.TName}} implements the FFT transform for vectors of {{.TType}}
type FFTV{{.TName}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TName}}      // Data input
	outs    []*core.InputV{{.TName}}    // array connected inputs
	bufs    [][]{{.TType}}              // output buffers
	bufi    int                         // current output buffer index
	forward bool                        // transform direction
	size    int                         // current size of the FFT
    fft     *FFTRadix2V{{.TName}}       // FFT algorithm implementation
}

// NewFFTV{{.TName}} creates and returns a pointer to a new FFTV{{.TName}} with the
// specified direction
func NewFFTV{{.TName}}(forward bool) *FFTV{{.TName}} {

	f := new(FFTV{{.TName}})
	f.forward = forward
	f.inpData = core.NewInputV{{.TName}}(f)
	return f
}

// Input returns the input with the specified name
// Returns nil if not found
func (f *FFTV{{.TName}}) Input(name string) interface{} {

    switch name {
    case "data":
        return f.inpData
    default:
        return nil
    }
    return nil
}

// Connect connects the specified input to this FFT output
func (f *FFTV{{.TName}}) Connect(inp interface{}) {

	f.outs = append(f.outs, inp.(*core.InputV{{.TName}}))
}

// Run reads input data, generates output buffer with the FFT
// of the input and send the result to connected inputs.
func (f *FFTV{{.TName}}) Run() {

	// Reads input data
	data := f.inpData.Read()

	// Initialize FFT transform
	size := len(data)
	if size != f.size || f.fft == nil {
		fft, err := NewFFTRadix2V{{.TName}}(size)
		if err != nil {
			panic(err)
		}
        f.fft = fft
		f.size = size
        log.Debug("FFT{{.TName}} initialized for size:%v", size)
	}

	// Reallocates output buffers if necessary
	f.bufs = core.ReallocBuffersV{{.TName}}(f.bufs, core.OutBuffers, size)

	// Generates FFT
    buf := f.bufs[f.bufi]
    if f.forward {
        f.fft.Forward(data, buf)
    } else {
        f.fft.Inverse(data, buf)
    }

	// Sends to connected inputs
	for i := 0; i < len(f.outs); i++ {
		f.outs[i].Write(buf[:size])
	}

	// Prepare to use next output buffer
	f.bufi++
	if f.bufi >= len(f.bufs) {
		f.bufi = 0
	}
}
{{end}}

