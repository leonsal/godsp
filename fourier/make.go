package fourier

//go:generate goxtempl -in=fft_radix2.t -out=gen_fft_radix2_c64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=fft_radix2.t -out=gen_fft_radix2_c128.go TName=C128 TType=complex128

//go:generate goxtempl -in=fft.t -out=gen_fft_c64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=fft.t -out=gen_fft_c128.go TName=C128 TType=complex128
