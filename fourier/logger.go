package fourier

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("FOURIER", logger.Default())
