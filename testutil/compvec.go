package testutil

import (
	"math"
	"testing"
)

const epsilon = 0.000001

func CompVINT(t *testing.T, exp, got []int) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != got[i] {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVI16(t *testing.T, exp, got []int16) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != got[i] {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVI32(t *testing.T, exp, got []int32) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != got[i] {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVF32(t *testing.T, exp, got []float32) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if math.Abs(float64(exp[i]-got[i])) > epsilon {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVF64(t *testing.T, exp, got []float64) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if math.Abs(exp[i]-got[i]) > epsilon {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVC64(t *testing.T, exp, got []complex64) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != got[i] {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}

func CompVC128(t *testing.T, exp, got []complex128) {

	if len(exp) != len(got) {
		t.Error("Expected length:%v different from result:%v", len(exp), len(got))
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != got[i] {
			t.Errorf("Expected value at:%v -> %v got:%v", i, exp[i], got[i])
		}
	}
}
