package wavegen

// File generated from template: signal.t

import (
	"math"
	"sync/atomic"

	"github.com/leonsal/godsp/core"
)

// SignalVF32 implements a waveform generator of buffer of type float32
type SignalVF32 struct {
	core.Node                       // Embedded node
	status        uint32            // run status
	inpFrequency  *core.InputUINT   // frequency input
	inpSamplerate *core.InputUINT   // sample rate input
	inpAmplitude  *core.InputF32    // amplitude input
	inpOffset     *core.InputF32    // offset input
	inpWaveform   *core.InputUINT   // waveform type input
	outs          []*core.InputVF32 // array of outputs
	framesize     int               // current frame size
	samplerate    uint              // current sample rate
	freq          uint              // current frequency
	amplitude     float32           // current amplitude
	offset        float32           // current offset
	phase         float64           // current phase
	waveform      uint              // current waveform function index
	wavefuncs     []func([]float32) // array of wave generation functions
	bufs          [][]float32       // output buffers
	bufi          int               // current output buffer index
	chClosed      chan struct{}
}

// NewSignalVF32 creates and returns a new signal source with the specified frame size
func NewSignalVF32(framesize uint) *SignalVF32 {

	s := new(SignalVF32)
	s.framesize = int(framesize)
	s.inpFrequency = core.NewInputUINT(s)
	s.inpSamplerate = core.NewInputUINT(s)
	s.inpAmplitude = core.NewInputF32(s)
	s.inpOffset = core.NewInputF32(s)
	s.inpWaveform = core.NewInputUINT(s)
	s.bufs = core.ReallocBuffersVF32(nil, core.OutBuffers, s.framesize)
	s.wavefuncs = []func([]float32){
		s.waveConst,
		s.waveSin,
		s.waveCos,
		s.waveSquare,
		s.waveTriangle,
		s.waveSawtooth,
	}
	s.chClosed = make(chan struct{})
	return s
}

// Input returns the input with the specified name
func (s *SignalVF32) Input(name string) interface{} {

	switch name {
	case "frequency":
		return s.inpFrequency
	case "samplerate":
		return s.inpSamplerate
	case "amplitude":
		return s.inpAmplitude
	case "offset":
		return s.inpOffset
	case "waveform":
		return s.inpWaveform
	default:
		return nil
	}
	return nil
}

// Connect connects the specified input to this signal output
func (s *SignalVF32) Connect(inp interface{}) {

	s.outs = append(s.outs, inp.(*core.InputVF32))
}

// Start starts the signal generator if not already started
func (s *SignalVF32) Start() error {

	// Can only start if it stopped
	if atomic.LoadUint32(&s.status) != core.Stopped {
		return nil
	}
	// Block outputs
	for i := 0; i < len(s.outs); i++ {
		s.outs[i].WriteBlock()
	}
	// Sets status flag controlling the goroutine
	atomic.StoreUint32(&s.status, core.Started)
	log.Debug("signal:%p START", s)
	go func() {
		for atomic.LoadUint32(&s.status) == core.Started {
			s.Run()
		}
		// Write to channel informing goroutine end.
		s.chClosed <- struct{}{}
	}()
	return nil
}

// Stop stops the signal generator if not already stopped
func (s *SignalVF32) Stop() error {

	// Can only stop if it is started
	if s.status != core.Started {
		return nil
	}
	// Sets status flag controlling the goroutine
	atomic.StoreUint32(&s.status, core.Stopped)
	// Unblock outputs
	for i := 0; i < len(s.outs); i++ {
		s.outs[i].WriteUnblock()
	}
	// Waits for goroutine to end.
	<-s.chClosed
	log.Debug("signal:%p STOPPED", s)
	return nil
}

// Close closes this signal generator
func (s *SignalVF32) Close() error {

	s.Stop()
	atomic.StoreUint32(&s.status, core.Closed)
	return nil
}

// Status returns the signal generator running status
func (s *SignalVF32) Status() uint32 {

	return atomic.LoadUint32(&s.status)
}

// Run generates one signal frame.
// It is normally executed by a goroutine in Start().
// This method is usually used for tests.
func (s *SignalVF32) Run() {

	// Read inputs
	s.freq = s.inpFrequency.Read()
	s.samplerate = s.inpSamplerate.Read()
	s.amplitude = s.inpAmplitude.Read()
	s.offset = s.inpOffset.Read()
	waveform := s.inpWaveform.Read()
	if int(waveform) > len(s.wavefuncs)-1 {
		panic("Invalid waveform")
	}
	if waveform != s.waveform {
		s.waveform = waveform
		s.phase = 0
	}

	// Generates output buffer data using the current waveform function
	buf := s.bufs[s.bufi]
	s.wavefuncs[s.waveform](buf)

	// Sends generated data to connected inputs
	// May block if previous data not read from destination input
	for i := 0; i < len(s.outs); i++ {
		s.outs[i].Write(buf)
	}

	// Prepare to use next output buffer
	s.bufi++
	if s.bufi >= len(s.bufs) {
		s.bufi = 0
	}
}

func (s *SignalVF32) waveConst(buf []float32) {

	for i := 0; i < s.framesize; i++ {
		buf[i] = s.amplitude + s.offset
	}
}

func (s *SignalVF32) waveSin(buf []float32) {

	const period = 2 * math.Pi
	delta := period / (float64(s.samplerate) / float64(s.freq))
	for i := 0; i < len(buf); i++ {
		buf[i] = float32(float64(s.amplitude)*math.Sin(float64(s.phase)) + float64(s.offset))
		s.phase += delta
		if s.phase > period {
			s.phase = s.phase - period
		}
	}
}

func (s *SignalVF32) waveCos(buf []float32) {

	const period = 2 * math.Pi
	delta := period / (float64(s.samplerate) / float64(s.freq))
	for i := 0; i < len(buf); i++ {
		buf[i] = float32(float64(s.amplitude)*math.Cos(float64(s.phase)) + float64(s.offset))
		s.phase += delta
		if s.phase > period {
			s.phase = s.phase - period
		}
	}
}

func (s *SignalVF32) waveSquare(buf []float32) {

	const period = 1.0
	delta := period / (float64(s.samplerate) / float64(s.freq))
	for i := 0; i < len(buf); i++ {
		if s.phase < period/2 {
			buf[i] = s.offset
		} else {
			buf[i] = s.amplitude + s.offset
		}
		s.phase += delta
		if s.phase >= period {
			s.phase = s.phase - period
		}
	}
}

func (s *SignalVF32) waveTriangle(buf []float32) {

	const period = 4.0
	delta := period / (float64(s.samplerate) / float64(s.freq))
	for i := 0; i < len(buf); i++ {
		var v float64
		if s.phase < 1 {
			v = s.phase
		} else if s.phase < 3 {
			v = 2 - s.phase
		} else {
			v = s.phase - 4
		}
		buf[i] = float32(v*float64(s.amplitude)) + s.offset
		s.phase += delta
		if s.phase >= period {
			s.phase = s.phase - period
		}
	}
}

func (s *SignalVF32) waveSawtooth(buf []float32) {

	const period = 1.0
	delta := period / (float64(s.samplerate) / float64(s.freq))
	for i := 0; i < len(buf); i++ {
		buf[i] = float32(s.phase*float64(s.amplitude) + float64(s.offset))
		s.phase += delta
		if s.phase >= period {
			s.phase = s.phase - period
		}
	}
}
