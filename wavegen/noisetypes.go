package wavegen

const (
	// Uniform specifies an uniform distribution
	Uniform = iota
	// Gaussian specifies aa Gaussian distribution
	Gaussian
)
