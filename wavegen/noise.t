package wavegen

import (
	"github.com/leonsal/godsp/core"
)

{{range .}}
// NoiseV{{.TName}} implements a waveform generator of buffer of type: {{.TType}}
type NoiseV{{.TName}} struct {
	core.Source                             // embedded source
	Amplitude   *core.Input{{.TName}}       // amplitude input
	Offset      *core.Input{{.TName}}       // offset input
	NoiseType   *core.InputUINT             // noise type input
	outs        []*core.InputV{{.TName}}    // array of outputs
	amplitude   {{.TType}}                  // current amplitude
	offset      {{.TType}}                  // current offset
	framesize   int                         // frame size
	noisetype   uint                        // current noise function index
    noisefuncs  []func([]{{.TType}})        // array of noise generation functions
	bufs        [][]{{.TType}}              // output buffers
	bufi        int                         // current output buffer index
}

// NewNoiseV{{.TName}} creates and returns a new signal source with the specified frame size
func NewNoise{{.TName}}(framesize uint) *NoiseV{{.TName}} {

	n := new(NoiseV{{.TName}})
	n.Source.Init()
	n.framesize = int(framesize)
	n.Amplitude = core.NewInput{{.TName}}()
	n.Offset = core.NewInput{{.TName}}()
	n.NoiseType = core.NewInputUINT()
	n.Amplitude.Set(1)
	n.Offset.Set(0)
	n.NoiseType.Set(0)
	n.bufs = core.ReallocBuffersV{{.TName}}(nil, core.OutBuffers, n.framesize)
    n.noisefuncs = []func([]{{.TType}}){
        n.noiseUniform,
        n.noiseGaussian,
    }
	go n.run()
	return n
}

// Connect connects the specified input to this signal output
func (n *NoiseV{{.TName}}) Connect(inp *core.InputV{{.TName}}) {

	n.outs = append(n.outs, inp)
}

// run is a goroutine started by Start() which generates
// waveform sample frames and sends to the connected inputs
func (n *NoiseV{{.TName}}) run() {

	for {
		// If signal source closed, breaks
		if n.Closed() {
			break
		}

		// Read inputs
		n.amplitude = n.Amplitude.Read()
		n.offset = n.Offset.Read()
		noisetype := n.NoiseType.Read()
		if int(noisetype) > len(n.noisefuncs)-1 {
			panic("Invalid noise type")
		}
		if noisetype != n.noisetype {
			n.noisetype = noisetype
		}

		// Generates output buffer data using the current noise function
		buf := n.bufs[n.bufi]
		n.noisefuncs[n.noisetype](buf)

		// Sends generated data to connected inputs
		// May block if previous data not read from destination input
		for i := 0; i < len(n.outs); i++ {
			n.outs[i].Write(buf)
		}

		// Prepare to use next output buffer
		n.bufi++
		if n.bufi >= len(n.bufs) {
			n.bufi = 0
		}
	}
	// Closes connected inputs
	for i := 0; i < len(n.outs); i++ {
		n.outs[i].Close()
	}
	log.Debug("Noise{{.TName}} finished")
}

func (n *NoiseV{{.TName}}) noiseUniform(buf []{{.TType}}) {

	for i := 0; i < n.framesize; i++ {
		buf[i] = n.amplitude + n.offset
	}
}

func (n *NoiseV{{.TName}}) noiseGaussian(buf []{{.TType}}) {

	for i := 0; i < n.framesize; i++ {
		buf[i] = n.amplitude + n.offset
	}
}


{{end}}

