package wavegen

const (
	// Constant specifies a constant waveform
	Constant = iota
	// Sine specifies a sine waveform
	Sine
	// Cosine specifies a cosine waveform
	Cosine
	// Square specifies a square waveform
	Square
	// Triangle specifies a triangle waveform
	Triangle
	// Sawtooth specifies a saw tooth waveform
	Sawtooth
)
