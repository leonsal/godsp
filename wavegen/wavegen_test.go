package wavegen

import (
	"fmt"
	"strings"
	"testing"

	"github.com/leonsal/godsp/util"
)

func TestSignalVI16(t *testing.T) {

	s := NewSignalVI16(32)
	s.Frequency.Set(1000)
	s.Samplerate.Set(8000)
	s.Amplitude.Set(30000)
	s.Offset.Set(0)

	d := util.NewDumperVI16(32, 16, " %6d")
	s.Connect(d.Input)

	fmt.Println("Const " + strings.Repeat("-", 132))
	s.Waveform.Set(Constant)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sine " + strings.Repeat("-", 132))
	s.Waveform.Set(Sine)

	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Cosine " + strings.Repeat("-", 132))
	s.Waveform.Set(Cosine)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Square " + strings.Repeat("-", 132))
	s.Waveform.Set(Square)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Triangle " + strings.Repeat("-", 132))
	s.Waveform.Set(Triangle)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sawtooth " + strings.Repeat("-", 132))
	s.Waveform.Set(Sawtooth)
	for i := 0; i < 2; i++ {
		s.Run()
	}
}

func TestSignalVF32(t *testing.T) {

	s := NewSignalVF32(32)
	s.Frequency.Set(1000)
	s.Samplerate.Set(8000)
	s.Amplitude.Set(1)
	s.Offset.Set(0)

	d := util.NewDumperVF32(32, 16, " %6.2f")
	s.Connect(d.Input)

	fmt.Println("Const " + strings.Repeat("-", 132))
	s.Waveform.Set(Constant)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sine " + strings.Repeat("-", 132))
	s.Waveform.Set(Sine)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Cosine " + strings.Repeat("-", 132))
	s.Waveform.Set(Cosine)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Square " + strings.Repeat("-", 132))
	s.Waveform.Set(Square)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Triangle " + strings.Repeat("-", 132))
	s.Waveform.Set(Triangle)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sawtooth " + strings.Repeat("-", 132))
	s.Waveform.Set(Sawtooth)
	for i := 0; i < 2; i++ {
		s.Run()
	}
}

func TestSignalVF64(t *testing.T) {

	s := NewSignalVF64(32)
	s.Frequency.Set(1000)
	s.Samplerate.Set(8000)
	s.Amplitude.Set(1)
	s.Offset.Set(0)

	d := util.NewDumperVF64(32, 16, " %6.3f")
	s.Connect(d.Input)

	fmt.Println("Const " + strings.Repeat("-", 132))
	s.Waveform.Set(Constant)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sine " + strings.Repeat("-", 132))
	s.Waveform.Set(Sine)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Cosine " + strings.Repeat("-", 132))
	s.Waveform.Set(Cosine)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Square " + strings.Repeat("-", 132))
	s.Waveform.Set(Square)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Triangle " + strings.Repeat("-", 132))
	s.Waveform.Set(Triangle)
	for i := 0; i < 2; i++ {
		s.Run()
	}
	fmt.Println("Sawtooth " + strings.Repeat("-", 132))
	s.Waveform.Set(Sawtooth)
	for i := 0; i < 2; i++ {
		s.Run()
	}
}
