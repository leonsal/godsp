package wavegen

//go:generate goxtempl -in=signal.t -out=gen_signal_vi16.go  TName=I16  TType=int16
//go:generate goxtempl -in=signal.t -out=gen_signal_vf32.go  TName=F32  TType=float32
//go:generate goxtempl -in=signal.t -out=gen_signal_vf64.go  TName=F64  TType=float64
