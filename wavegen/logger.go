package wavegen

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("WAVEGEN", logger.Default())
