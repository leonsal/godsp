package operator

import (
	"strconv"
	"strings"

	"github.com/leonsal/godsp/core"
)

// MultVC64 implements the multiplication of an arbitrary number of vector and scalar inputs
// of complex64 and sends the result vector to its connected outputs
type MultVC64 struct {
	core.Node                   // Embedded node
	inpv      []*core.InputVC64 // array of vector inputs
	inps      []*core.InputC64  // array of scalar inputs
	outs      []*core.InputVC64 // array of outputs
	bufs      [][]complex64     // output buffers
	bufi      int               // current output buffer index
	inpvData  [][]complex64     // array of vector input data read
	inpsData  []complex64       // array of scalar input data read
}

// NewMultVC64 creates and returns a pointer to a new MultVC64
func NewMultVC64(ninpv, ninps int) *MultVC64 {

	a := new(MultVC64)
	// Create vector inputs
	for i := 0; i < ninpv; i++ {
		a.inpv = append(a.inpv, core.NewInputVC64(a))
	}
	// Create scalar inputs
	for i := 0; i < ninps; i++ {
		a.inps = append(a.inps, core.NewInputC64(a))
	}
	a.inpvData = make([][]complex64, ninpv)
	a.inpsData = make([]complex64, ninps)
	return a
}

// Input returns the input with the specified name
// The scalar inputs are named: s0, s1, ... sn
// The vector inputs are named: v0, v1, ... vn
// Returns nil if not found
func (a *MultVC64) Input(name string) interface{} {

	// Get input number
	if len(name) < 2 {
		return nil
	}
	n, err := strconv.Atoi(name[1:])
	if err != nil {
		return nil
	}

	// Scalar input
	if strings.HasPrefix(name, "s") {
		return a.inps[n]
	}
	// Vector input
	if strings.HasPrefix(name, "v") {
		return a.inpv[n]
	}
	return nil
}

// Connect connects this adder output to the specified input
func (a *MultVC64) Connect(inp interface{}) {

	a.outs = append(a.outs, inp.(*core.InputVC64))
}

// Run checks if all inputs are valid and if true generates output buffer
// with the multiplication of the input buffers and writes the result to the
// connected outputs.
func (a *MultVC64) Run() {

	// Read all vector inputs
	framesize := 0
	for i := 0; i < len(a.inpv); i++ {
		data := a.inpv[i].Read()
		// Check inputs length
		if i == 0 {
			framesize = len(data)
		} else {
			if framesize != len(data) {
				log.Error("framesize:%v inplen:%v", framesize, len(data))
				panic("Inputs with different lengths")
			}
		}
		a.inpvData[i] = data
	}

	// Read all scalar inputs
	for i := 0; i < len(a.inps); i++ {
		data := a.inps[i].Read()
		a.inpsData[i] = data
	}

	// Reallocates output buffers if necessary
	a.bufs = core.ReallocBuffersVC64(a.bufs, core.OutBuffers, framesize)

	// Multiply inputs to output buffer
	buf := a.bufs[a.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = 1
		// Multiply by vector inputs
		for j := 0; j < len(a.inpv); j++ {
			buf[i] *= a.inpvData[j][i]
		}
		// Multiply by scalar inputs
		for j := 0; j < len(a.inps); j++ {
			buf[i] *= a.inpsData[j]
		}
	}

	// Sends output to connected inputs
	for i := 0; i < len(a.outs); i++ {
		a.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	a.bufi++
	if a.bufi >= len(a.bufs) {
		a.bufi = 0
	}
}
