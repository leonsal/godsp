package operator

import (
	"testing"

	"github.com/leonsal/godsp/testutil"
	"github.com/leonsal/godsp/util"
)

func TestMultVI16(t *testing.T) {

	// Test data
	v0 := []int16{0, 10, 20, 30, 40, 50, 60, 70}
	v1 := []int16{0, 1, 2, 3, 4, 5, 6, 7}
	s0 := int16(2)
	res := []int16{0, 20, 80, 180, 320, 500, 720, 980}

	// Creates multiplier
	m := NewMultVI16(2, 1)
	tap := util.NewTapVI16()
	m.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(buf []int16) {
		testutil.CompVI16(t, buf, res)
		count++
		if count > 1 {
			t.Fatal("Multiplier should not have executed")
		}
	})
	m.Inputs(0).Unset()

	// Write inputs
	m.Inputv(0).Write(v0)
	m.Inputv(1).Write(v1)
	m.Inputs(0).Write(s0)

	// This partial inputs should not generate a result
	m.Inputv(1).Write(v0)
	m.Inputs(0).Write(s0)
}
