package operator

import (
	"github.com/leonsal/godsp/core"
	"strconv"
)

// AdderVF32 implements the sum of an arbitrary number of vector inputs
// of float32 and sends the result vector to its connected outputs
type AdderVF32 struct {
	core.Node                   // Embedded node
	inps      []*core.InputVF32 // array of inputs
	outs      []*core.InputVF32 // array of connected inputs
	bufs      [][]float32       // output buffers
	bufi      int               // current output buffer index
	inpData   [][]float32       // array of input data read
}

// NewAdderVF32 creates and returns a pointer to a new AdderVF32
// with the specified number of inputs.
func NewAdderVF32(ninps int) *AdderVF32 {

	a := new(AdderVF32)
	for i := 0; i < ninps; i++ {
		a.inps = append(a.inps, core.NewInputVF32(a))
	}
	a.inpData = make([][]float32, ninps)
	return a
}

// Input returns the input with the specified name
// Returns nil if not found
func (a *AdderVF32) Input(name string) interface{} {

	n, err := strconv.Atoi(name)
	if err != nil {
		return nil
	}
	return a.inps[n]
}

// Connect connects this adder output to the specified input
func (a *AdderVF32) Connect(inp interface{}) {

	a.outs = append(a.outs, inp.(*core.InputVF32))
}

// Run reads all inputs, sums its values in an output buffer
// and then sends the output buffer to connected inputs.
func (a *AdderVF32) Run() {

	// Read all inputs
	framesize := 0
	for i := 0; i < len(a.inps); i++ {
		data := a.inps[i].Read()
		// Check inputs length
		if i == 0 {
			framesize = len(data)
		} else {
			if framesize != len(data) {
				panic("Inputs with different lengths")
			}
		}
		a.inpData[i] = data
	}

	// Reallocates output buffers if necessary
	a.bufs = core.ReallocBuffersVF32(a.bufs, core.OutBuffers, framesize)

	// Sum inputs to output buffer
	buf := a.bufs[a.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = 0
		for j := 0; j < len(a.inps); j++ {
			buf[i] += a.inpData[j][i]
		}
	}

	// Sends output to connected inputs
	for i := 0; i < len(a.outs); i++ {
		a.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	a.bufi++
	if a.bufi >= len(a.bufs) {
		a.bufi = 0
	}
}
