package operator

// Adder
//go:generate goxtempl -in=adder.t -out=gen_adder_int.go  TName=INT  TType=int
//go:generate goxtempl -in=adder.t -out=gen_adder_i16.go  TName=I16  TType=int16
//go:generate goxtempl -in=adder.t -out=gen_adder_f32.go  TName=F32  TType=float32
//go:generate goxtempl -in=adder.t -out=gen_adder_f64.go  TName=F64  TType=float64
//go:generate goxtempl -in=adder.t -out=gen_adder_c64.go  TName=C64  TType=complex64
//go:generate goxtempl -in=adder.t -out=gen_adder_c128.go TName=C128 TType=complex128

// Multiplier
//go:generate goxtempl -in=mult.t  -out=gen_mult_int.go   TName=INT  TType=int
//go:generate goxtempl -in=mult.t  -out=gen_mult_i16.go   TName=I16  TType=int16
//go:generate goxtempl -in=mult.t  -out=gen_mult_f32.go   TName=F32  TType=float32
//go:generate goxtempl -in=mult.t  -out=gen_mult_f64.go   TName=F64  TType=float64
//go:generate goxtempl -in=mult.t  -out=gen_mult_c64.go   TName=C64  TType=complex64
//go:generate goxtempl -in=mult.t  -out=gen_mult_c128.go  TName=C128 TType=complex128
