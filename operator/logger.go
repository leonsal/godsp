package operator

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("OPER", logger.Default())
