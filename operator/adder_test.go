package operator

import (
	"testing"

	"github.com/leonsal/godsp/testutil"
	"github.com/leonsal/godsp/util"
)

func TestAdderVI16(t *testing.T) {

	// Test data
	i1 := []int16{0, 10, 20, 30, 40, 50, 60, 70}
	i2 := []int16{0, 1, 2, 3, 4, 5, 6, 7}
	i3 := []int16{1, 2, 3, 4, 5, 6, 7, 8}
	sum := []int16{1, 13, 25, 37, 49, 61, 73, 85}

	// Creates adder and connects its output to a tap
	a := NewAdderVI16(3)
	tap := util.NewTapVI16()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []int16) {
		testutil.CompVI16(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	a.Input(2).Write(i3)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}

func TestAdderVINT(t *testing.T) {

	// Test data
	i1 := []int{0, 10, 20, 30, 40, 50, 60, 70}
	i2 := []int{0, 1, 2, 3, 4, 5, 6, 7}
	i3 := []int{1, 2, 3, 4, 5, 6, 7, 8}
	sum := []int{1, 13, 25, 37, 49, 61, 73, 85}

	// Creates adder and connects its output to a tap
	a := NewAdderVINT(3)
	tap := util.NewTapVINT()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []int) {
		testutil.CompVINT(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(2).Write(i3)
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}

func TestAdderVF32(t *testing.T) {

	// Test data
	i1 := []float32{0, 1, 2, 3, 4, 5, 6, 7}
	i2 := []float32{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7}
	i3 := []float32{1, 2, 3, 4, 5, 6, 7, 8}
	sum := []float32{1, 3.1, 5.2, 7.3, 9.4, 11.5, 13.6, 15.7}

	// Creates adder and connects its output to a tap
	a := NewAdderVF32(3)
	tap := util.NewTapVF32()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []float32) {
		testutil.CompVF32(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(2).Write(i3)
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}

func TestAdderVF64(t *testing.T) {

	// Test data
	i1 := []float64{0, 1, 2, 3, 4, 5, 6, 7}
	i2 := []float64{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7}
	i3 := []float64{1, 2, 3, 4, 5, 6, 7, 8}
	sum := []float64{1, 3.1, 5.2, 7.3, 9.4, 11.5, 13.6, 15.7}

	// Creates adder and connects its output to a tap
	a := NewAdderVF64(3)
	tap := util.NewTapVF64()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []float64) {
		testutil.CompVF64(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(2).Write(i3)
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}

func TestAdderVC64(t *testing.T) {

	// Test data
	i1 := []complex64{0, 1 + 1i, 2 + 2i, 3 + 3i, 4 + 4i, 5 + 5i, 6 + 6i, 7 + 7i}
	i2 := []complex64{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7}
	i3 := []complex64{1, 0.1i, 0.2i, 0.3i, 0.4i, 0.5i, 0.6i, 0.7i}
	sum := []complex64{1, 1.1 + 1.1i, 2.2 + 2.2i, 3.3 + 3.3i, 4.4 + 4.4i, 5.5 + 5.5i, 6.6 + 6.6i, 7.7 + 7.7i}

	// Creates adder and connects its output to a tap
	a := NewAdderVC64(3)
	tap := util.NewTapVC64()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []complex64) {
		testutil.CompVC64(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(2).Write(i3)
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}

func TestAdderVC128(t *testing.T) {

	// Test data
	i1 := []complex128{0, 1 + 1i, 2 + 2i, 3 + 3i, 4 + 4i, 5 + 5i, 6 + 6i, 7 + 7i}
	i2 := []complex128{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7}
	i3 := []complex128{1, 0.1i, 0.2i, 0.3i, 0.4i, 0.5i, 0.6i, 0.7i}
	sum := []complex128{1, 1.1 + 1.1i, 2.2 + 2.2i, 3.3 + 3.3i, 4.4 + 4.4i, 5.5 + 5.5i, 6.6 + 6.6i, 7.7 + 7.7i}

	// Creates adder and connects its output to a tap
	a := NewAdderVC128(3)
	tap := util.NewTapVC128()
	a.Connect(tap.Input)
	count := 0
	tap.SetCallback(func(res []complex128) {
		testutil.CompVC128(t, sum, res)
		count++
		if count > 1 {
			t.Fatal("Adder should not have executed")
		}
	})

	// Write inputs
	a.Input(2).Write(i3)
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
	// Write incomplete inputs
	a.Input(0).Write(i1)
	a.Input(1).Write(i2)
}
