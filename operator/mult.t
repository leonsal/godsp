package operator

import (
    "strconv"
    "strings"

	"github.com/leonsal/godsp/core"
)

{{range .}}
// MultV{{.TName}} implements the multiplication of an arbitrary number of vector and scalar inputs
// of {{.TType}} and sends the result vector to its connected outputs
type MultV{{.TName}} struct {
    core.Node                           // Embedded node
	inpv     []*core.InputV{{.TName}}   // array of vector inputs
	inps     []*core.Input{{.TName}}    // array of scalar inputs
	outs     []*core.InputV{{.TName}}   // array of outputs
	bufs     [][]{{.TType}}             // output buffers
	bufi     int                        // current output buffer index
    inpvData [][]{{.TType}}             // array of vector input data read
    inpsData []{{.TType}}               // array of scalar input data read
}

// NewMultV{{.TName}} creates and returns a pointer to a new MultV{{.TName}}
func NewMultV{{.TName}}(ninpv, ninps int) *MultV{{.TName}} {

	a := new(MultV{{.TName}})
    // Create vector inputs
	for i := 0; i < ninpv; i++ {
		a.inpv = append(a.inpv, core.NewInputV{{.TName}}(a))
	}
    // Create scalar inputs
	for i := 0; i < ninps; i++ {
		a.inps = append(a.inps, core.NewInput{{.TName}}(a))
	}
	a.inpvData = make([][]{{.TType}}, ninpv)
	a.inpsData = make([]{{.TType}}, ninps)
	return a
}

// Input returns the input with the specified name
// The scalar inputs are named: s0, s1, ... sn
// The vector inputs are named: v0, v1, ... vn
// Returns nil if not found
func (a *MultV{{.TName}}) Input(name string) interface{} {

    // Get input number
    if len(name) < 2 {
        return nil
    }
    n, err := strconv.Atoi(name[1:])
    if err != nil {
        return nil
    }

    // Scalar input
    if strings.HasPrefix(name, "s") {
	    return a.inps[n]
    }
    // Vector input
    if strings.HasPrefix(name, "v") {
	    return a.inpv[n]
    }
    return nil
}

// Connect connects this adder output to the specified input
func (a *MultV{{.TName}}) Connect(inp interface{}) {

	a.outs = append(a.outs, inp.(*core.InputV{{.TName}}))
}

// Run checks if all inputs are valid and if true generates output buffer
// with the multiplication of the input buffers and writes the result to the
// connected outputs.
func (a *MultV{{.TName}}) Run() {

	// Read all vector inputs
	framesize := 0
	for i := 0; i < len(a.inpv); i++ {
		data := a.inpv[i].Read()
		// Check inputs length
		if i == 0 {
			framesize = len(data)
		} else {
			if framesize != len(data) {
                log.Error("framesize:%v inplen:%v", framesize, len(data))
				panic("Inputs with different lengths")
			}
		}
        a.inpvData[i] = data
	}

    // Read all scalar inputs
	for i := 0; i < len(a.inps); i++ {
        data := a.inps[i].Read()
		a.inpsData[i] = data
    }

	// Reallocates output buffers if necessary
	a.bufs = core.ReallocBuffersV{{.TName}}(a.bufs, core.OutBuffers, framesize)

	// Multiply inputs to output buffer
    buf := a.bufs[a.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = 1
        // Multiply by vector inputs
		for j := 0; j < len(a.inpv); j++ {
			buf[i] *= a.inpvData[j][i]
		}
        // Multiply by scalar inputs
		for j := 0; j < len(a.inps); j++ {
			buf[i] *= a.inpsData[j]
		}
	}

	// Sends output to connected inputs
	for i := 0; i < len(a.outs); i++ {
		a.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	a.bufi++
	if a.bufi >= len(a.bufs) {
		a.bufi = 0
	}
}

{{end}}


