package convert

import (
	"github.com/leonsal/godsp/core"
)

// VI16toVF64 implements a node which convert an input vector of int16
// to an output vector of float64
type VI16toVF64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVI16   // Data input
	outs      []*core.InputVF64 // inputs connected to the output
	bufs      [][]float64       // output buffers
	bufi      int               // current output buffer index
}

// NewVI16toVF64 creates and returns a pointer to a new converter
func NewVI16toVF64() *VI16toVF64 {

	c := new(VI16toVF64)
	c.inpData = core.NewInputVI16(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VI16toVF64) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VI16toVF64) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVF64))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VI16toVF64) Run() {

	// Read input
	data := c.inpData.Read()

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersVF64(c.bufs, core.OutBuffers, len(data))

	// Convert input vector type to output vector type
	buf := c.bufs[c.bufi]
	framesize := len(data)
	for i := 0; i < framesize; i++ {
		buf[i] = float64(data[i])
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}

}
