package convert

import (
	"github.com/leonsal/godsp/core"
)

// VF64toVF64 implements a node which convert an input vector of float64
// to an output vector of float64
type VF64toVF64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF64   // Data input
	outs      []*core.InputVF64 // inputs connected to the output
}

// NewVF64toVF64 creates and returns a pointer to a new converter
func NewVF64toVF64() *VF64toVF64 {

	c := new(VF64toVF64)
	c.inpData = core.NewInputVF64(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VF64toVF64) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VF64toVF64) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVF64))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VF64toVF64) Run() {

	// Read input
	data := c.inpData.Read()

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(data)
	}

}
