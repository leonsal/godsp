package convert

import (
    "math"
	"github.com/leonsal/godsp/core"
)

{{range .}}
// V{{.TNameIn}}Mag{{.TNameOut}} implements a node which convert an input vector of {{.TTypeIn}}
// to an output vector of {{.TTypeOut}} magnitudes
type V{{.TNameIn}}Mag{{.TNameOut}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TNameIn}}    // Data input
	outs    []*core.InputV{{.TNameOut}} // inputs connected to the output
	bufs    [][]{{.TTypeOut}}           // output buffers
	bufi    int                         // current output buffer index
}

// NewV{{.TNameIn}}Mag{{.TNameOut}} creates and returns a pointer to a new converter
func NewV{{.TNameIn}}Mag{{.TNameOut}}() *V{{.TNameIn}}Mag{{.TNameOut}} {

	c := new(V{{.TNameIn}}Mag{{.TNameOut}})
	c.inpData = core.NewInputV{{.TNameIn}}(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *V{{.TNameIn}}Mag{{.TNameOut}}) Input(name string) interface{} {

    switch name {
    case "", "data":
        return c.inpData
    default:
        return nil
    }
}

// Connect this converter output to the specified input
func (c *V{{.TNameIn}}Mag{{.TNameOut}}) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputV{{.TNameOut}}))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *V{{.TNameIn}}Mag{{.TNameOut}}) Run() {

	// Read input
	data := c.inpData.Read()

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersV{{.TNameOut}}(c.bufs, core.OutBuffers, len(data))

	// Convert input data to magnitude
	framesize := len(data)
    buf := c.bufs[c.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = {{.TTypeOut}}(math.Sqrt(math.Pow(float64(real(data[i])), 2) + math.Pow(float64(imag(data[i])), 2)))
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}
}

{{end}}

