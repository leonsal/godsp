package convert

import (
	"github.com/leonsal/godsp/core"
)

{{range .}}
// V{{.TNameIn}}toV{{.TNameOut}} implements a node which convert an input vector of {{.TTypeIn}}
// to an output vector of {{.TTypeOut}}
type V{{.TNameIn}}toV{{.TNameOut}} struct {
    core.Node                           // Embedded node
	inpData *core.InputV{{.TNameIn}}    // Data input
	outs    []*core.InputV{{.TNameOut}} // inputs connected to the output
    {{if not (eq .TTypeIn .TTypeOut) -}}
	bufs    [][]{{.TTypeOut}}           // output buffers
	bufi    int                         // current output buffer index
    {{end -}}
}

// NewV{{.TNameIn}}toV{{.TNameOut}} creates and returns a pointer to a new converter
func NewV{{.TNameIn}}toV{{.TNameOut}}() *V{{.TNameIn}}toV{{.TNameOut}} {

	c := new(V{{.TNameIn}}toV{{.TNameOut}})
	c.inpData = core.NewInputV{{.TNameIn}}(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *V{{.TNameIn}}toV{{.TNameOut}}) Input(name string) interface{} {

    switch name {
    case "", "data":
        return c.inpData
    default:
        return nil
    }
}

// Connect this converter output to the specified input
func (c *V{{.TNameIn}}toV{{.TNameOut}}) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputV{{.TNameOut}}))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *V{{.TNameIn}}toV{{.TNameOut}}) Run() {

	// Read input
	data := c.inpData.Read()

    {{if eq .TTypeIn .TTypeOut}}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(data)
	}

    {{else}}

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersV{{.TNameOut}}(c.bufs, core.OutBuffers, len(data))

	// Convert input vector type to output vector type
    buf := c.bufs[c.bufi]
	framesize := len(data)
	for i := 0; i < framesize; i++ {
        {{if eq .TNameOut "C64" -}}
        buf[i] = complex64(complex(data[i], 0))
        {{else if eq .TNameOut "C128" -}}
        buf[i] = complex128(complex(data[i], 0))
        {{else -}}
        buf[i] = {{.TTypeOut}}(data[i])
        {{end -}}
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}

    {{end}}
}

{{end}}
