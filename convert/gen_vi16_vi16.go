package convert

import (
	"github.com/leonsal/godsp/core"
)

// VI16toVI16 implements a node which convert an input vector of int16
// to an output vector of int16
type VI16toVI16 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVI16   // Data input
	outs      []*core.InputVI16 // inputs connected to the output
}

// NewVI16toVI16 creates and returns a pointer to a new converter
func NewVI16toVI16() *VI16toVI16 {

	c := new(VI16toVI16)
	c.inpData = core.NewInputVI16(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VI16toVI16) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VI16toVI16) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVI16))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VI16toVI16) Run() {

	// Read input
	data := c.inpData.Read()

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(data)
	}

}
