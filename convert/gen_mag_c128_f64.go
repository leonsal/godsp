package convert

import (
	"github.com/leonsal/godsp/core"
	"math"
)

// VC128MagF64 implements a node which convert an input vector of complex128
// to an output vector of float64 magnitudes
type VC128MagF64 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVC128  // Data input
	outs      []*core.InputVF64 // inputs connected to the output
	bufs      [][]float64       // output buffers
	bufi      int               // current output buffer index
}

// NewVC128MagF64 creates and returns a pointer to a new converter
func NewVC128MagF64() *VC128MagF64 {

	c := new(VC128MagF64)
	c.inpData = core.NewInputVC128(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VC128MagF64) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VC128MagF64) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVF64))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VC128MagF64) Run() {

	// Read input
	data := c.inpData.Read()

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersVF64(c.bufs, core.OutBuffers, len(data))

	// Convert input data to magnitude
	framesize := len(data)
	buf := c.bufs[c.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = float64(math.Sqrt(math.Pow(float64(real(data[i])), 2) + math.Pow(float64(imag(data[i])), 2)))
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}
}
