package convert

import (
	"github.com/leonsal/godsp/core"
)

// VF32toVF32 implements a node which convert an input vector of float32
// to an output vector of float32
type VF32toVF32 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVF32   // Data input
	outs      []*core.InputVF32 // inputs connected to the output
}

// NewVF32toVF32 creates and returns a pointer to a new converter
func NewVF32toVF32() *VF32toVF32 {

	c := new(VF32toVF32)
	c.inpData = core.NewInputVF32(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VF32toVF32) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VF32toVF32) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVF32))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VF32toVF32) Run() {

	// Read input
	data := c.inpData.Read()

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(data)
	}

}
