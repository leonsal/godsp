package convert

import (
	"github.com/leonsal/godsp/core"
	"math"
)

// VC64MagF32 implements a node which convert an input vector of complex64
// to an output vector of float32 magnitudes
type VC64MagF32 struct {
	core.Node                   // Embedded node
	inpData   *core.InputVC64   // Data input
	outs      []*core.InputVF32 // inputs connected to the output
	bufs      [][]float32       // output buffers
	bufi      int               // current output buffer index
}

// NewVC64MagF32 creates and returns a pointer to a new converter
func NewVC64MagF32() *VC64MagF32 {

	c := new(VC64MagF32)
	c.inpData = core.NewInputVC64(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VC64MagF32) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VC64MagF32) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVF32))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VC64MagF32) Run() {

	// Read input
	data := c.inpData.Read()

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersVF32(c.bufs, core.OutBuffers, len(data))

	// Convert input data to magnitude
	framesize := len(data)
	buf := c.bufs[c.bufi]
	for i := 0; i < framesize; i++ {
		buf[i] = float32(math.Sqrt(math.Pow(float64(real(data[i])), 2) + math.Pow(float64(imag(data[i])), 2)))
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}
}
