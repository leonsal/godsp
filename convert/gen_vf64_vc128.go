package convert

import (
	"github.com/leonsal/godsp/core"
)

// VF64toVC128 implements a node which convert an input vector of float64
// to an output vector of complex128
type VF64toVC128 struct {
	core.Node                    // Embedded node
	inpData   *core.InputVF64    // Data input
	outs      []*core.InputVC128 // inputs connected to the output
	bufs      [][]complex128     // output buffers
	bufi      int                // current output buffer index
}

// NewVF64toVC128 creates and returns a pointer to a new converter
func NewVF64toVC128() *VF64toVC128 {

	c := new(VF64toVC128)
	c.inpData = core.NewInputVF64(c)
	return c
}

// Input returns the input with the specified name.
// Returns nil if not found.
func (c *VF64toVC128) Input(name string) interface{} {

	switch name {
	case "", "data":
		return c.inpData
	default:
		return nil
	}
}

// Connect this converter output to the specified input
func (c *VF64toVC128) Connect(inp interface{}) {

	c.outs = append(c.outs, inp.(*core.InputVC128))
}

// Run checks if all inputs are valid and if true
// converts the input data and writes the result buffer to
// the connected outputs.
func (c *VF64toVC128) Run() {

	// Read input
	data := c.inpData.Read()

	// Reallocates output buffers if necessary
	c.bufs = core.ReallocBuffersVC128(c.bufs, core.OutBuffers, len(data))

	// Convert input vector type to output vector type
	buf := c.bufs[c.bufi]
	framesize := len(data)
	for i := 0; i < framesize; i++ {
		buf[i] = complex128(complex(data[i], 0))
	}

	// Sends output to connected inputs
	for i := 0; i < len(c.outs); i++ {
		c.outs[i].Write(buf[:framesize])
	}

	// Prepare to use next output buffer
	c.bufi++
	if c.bufi >= len(c.bufs) {
		c.bufi = 0
	}

}
