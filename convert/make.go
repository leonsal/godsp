package convert

//go:generate goxtempl -in=cmplx2mag.t -out=gen_mag_c64_f32.go  TNameIn=C64  TTypeIn=complex64  TNameOut=F32 TTypeOut=float32
//go:generate goxtempl -in=cmplx2mag.t -out=gen_mag_c128_f64.go TNameIn=C128 TTypeIn=complex128 TNameOut=F64 TTypeOut=float64

//go:generate goxtempl -in=convert.t -out=gen_vi16_vi16.go TNameIn=I16 TTypeIn=int16   TNameOut=I16 TTypeOut=int16
//go:generate goxtempl -in=convert.t -out=gen_vf32_vf32.go TNameIn=F32 TTypeIn=float32 TNameOut=F32 TTypeOut=float32
//go:generate goxtempl -in=convert.t -out=gen_vf64_vf64.go TNameIn=F64 TTypeIn=float64 TNameOut=F64 TTypeOut=float64

//go:generate goxtempl -in=convert.t -out=gen_vi16_vf32.go TNameIn=I16 TTypeIn=int16   TNameOut=F32 TTypeOut=float32
//go:generate goxtempl -in=convert.t -out=gen_vi16_vf64.go TNameIn=I16 TTypeIn=int16   TNameOut=F64 TTypeOut=float64
//go:generate goxtempl -in=convert.t -out=gen_vf32_vf64.go TNameIn=F32 TTypeIn=float32 TNameOut=F64 TTypeOut=float64
//go:generate goxtempl -in=convert.t -out=gen_vf64_vf32.go TNameIn=F64 TTypeIn=float64 TNameOut=F32 TTypeOut=float32

//go:generate goxtempl -in=convert.t -out=gen_vf32_vc64.go  TNameIn=F32 TTypeIn=float32 TNameOut=C64  TTypeOut=complex64
//go:generate goxtempl -in=convert.t -out=gen_vf32_vc128.go TNameIn=F32 TTypeIn=float32 TNameOut=C128 TTypeOut=complex128
//go:generate goxtempl -in=convert.t -out=gen_vf64_vc64.go  TNameIn=F64 TTypeIn=float64 TNameOut=C64  TTypeOut=complex64
//go:generate goxtempl -in=convert.t -out=gen_vf64_vc128.go TNameIn=F64 TTypeIn=float64 TNameOut=C128 TTypeOut=complex128
