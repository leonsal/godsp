package radio

import (
	"unsafe"

	"github.com/leonsal/goairspy"
	"github.com/leonsal/godsp/core"
)

// Airspy implement a DSP Source for the airspy radio
type Airspy struct {
	core.Node                       // Embedded node
	inpFrequency  *core.InputUINT   // Frequency input
	inpSampleRate *core.InputUINT   // Sample rate input
	dev           airspy.Device     // Airspy device
	cfg           AirspyConfig      // Configuration
	opened        bool              // Device opened flag
	started       bool              // Reception started flag
	done          chan struct{}     // Channel for signaling end of goroutine
	outs          []*core.InputVC64 // Array of connected outputs
	bufs          [][]complex64     // Output buffers
	bufi          int               // Current output buffer index
}

// AirspyConfig describes the initial radio configuration used when opening the device.
type AirspyConfig struct {
	Serial uint64 // Device serial number. If zero the next available device will be opened
}

// NewAirspy creates and returns a new Airspy source adapter
// for the specified source and serial number.
// If the serial number is zero, the next closed device will be opened.
func NewAirspy(cfg *AirspyConfig) (*Airspy, error) {

	// Opens device
	r := new(Airspy)
	var err error
	if cfg.Serial == 0 {
		err = airspy.Open(&r.dev)
	} else {
		err = airspy.OpenSn(&r.dev, cfg.Serial)
	}
	if err != nil {
		return nil, err
	}

	// Sample type is always Float32IQ (complex64)
	err = airspy.SetSampleType(&r.dev, airspy.SampleFloat32IQ)
	if err != nil {
		return nil, err
	}

	// Sets default sample rate
	err = airspy.SetSampleRate(&r.dev, 10000000)
	if err != nil {
		return nil, err
	}

	// Create inputs
	r.inpFrequency = core.NewInputUINT(r)
	r.inpSampleRate = core.NewInputUINT(r)
	r.opened = true
	r.done = make(chan struct{})
	return r, nil
}

// GetCaps returns this radio capabilities
func (r *Airspy) GetCaps() *Caps {

	var caps Caps
	return &caps
}

// Input returns the input with the specified name
func (r *Airspy) Input(name string) interface{} {

	switch name {
	case "frequency":
		return r.inpFrequency
	case "samplerate":
		return r.inpSampleRate
	default:
		return nil
	}
}

// Connect connects the output of the radio to the specified input
func (r *Airspy) Connect(inp interface{}) {

	r.outs = append(r.outs, inp.(*core.InputVC64))
}

// Run satisfies the core.INode interface
func (r *Airspy) Run() {

}

// Start starts the radio reception
func (r *Airspy) Start() error {

	if r.started {
		log.Debug("Already started")
		return nil
	}

	err := airspy.StartRx(&r.dev, func(t *airspy.Transfer) int {
		// Sends received IQ data to connected outputs
		// May block if previous data not read from destination output
		slice := (*[1 << 30]complex64)(unsafe.Pointer(t.Samples))[:t.SampleCount]
		//log.Debug("Received:%d bytes data:%v", t.SampleCount, slice[:4])
		for i := 0; i < len(r.outs); i++ {
			r.outs[i].Write(slice)
		}
		return 0
	})
	if err != nil {
		return err
	}
	r.started = true
	// Start goroutine for reading inputs
	go r.readInputs()
	return nil
}

// Stop stops the radio reception
func (r *Airspy) Stop() error {

	if !r.started {
		log.Debug("Already stopped")
		return nil
	}
	// Stop goroutine for reading inputs
	r.done <- struct{}{}
	// Stops the radio reception
	err := airspy.StopRx(&r.dev)
	r.started = false
	return err
}

// Close closes the radio
func (r *Airspy) Close() error {

	r.Stop()
	err := airspy.Close(&r.dev)
	r.opened = false
	r.started = false
	return err
}

// Status returns the running status of this radio
func (r *Airspy) Status() uint32 {

	if !r.opened {
		return core.Closed
	}
	if r.started {
		return core.Started
	}
	return core.Stopped
}

// readInputs runs in a goroutine when the radio is started and is
// responsible to read the source inputs and sets its values to the radio.
// Must be finished when the radio is stopped or closed by writing to done channel.
func (r *Airspy) readInputs() {

loop:
	for {
		select {
		// Control channel
		case <-r.done:
			break loop
		// Frequency input channel
		case freq := <-r.inpFrequency.Chan():
			err := airspy.SetFreq(&r.dev, uint32(freq))
			if err != nil {
				log.Error("Error setting frequency:%v", err)
			} else {
				log.Debug("Frequency set:%v", freq)
			}
		}
	}
	log.Debug("readInputs finished")
}
