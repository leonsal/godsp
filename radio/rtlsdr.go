package radio

import (
	"github.com/leonsal/godsp/core"
	rtl "github.com/leonsal/gortlsdr"
)

// Rtlsdr implement a DSP Source for the rtlsdr radio
type Rtlsdr struct {
	inpFrequency  *core.InputUINT   // Frequency input
	inpSampleRate *core.InputUINT   // Sample rate input
	core.Node                       // Embedded node
	dev           *rtl.Device       // RTLSDR device
	cfg           RtlsdrConfig      // Configuration
	done          chan struct{}     // Channel for signaling end of goroutine
	started       bool              // Reception started flag
	outs          []*core.InputVC64 // Array of connected outputs
	bufs          [][]complex64     // Output buffers
	bufi          int               // Current output buffer index
}

// RtlsdrConfig specifies the configuration of the radio
type RtlsdrConfig struct {
	// Radio index (default= 0)
	Index uint
	// Number of reception buffers
	// If 0 the default=8 will be used
	Bufcount uint
	// Reception buffer size in bytes. Must be a multiple of 16K bytes (2^14)
	// If 0 the default=262144  will be used
	Bufsize uint
}

// NewRtlsdr creates and returns a new RTL-SDR source
func NewRtlsdr(cfg *RtlsdrConfig) (*Rtlsdr, error) {

	// Opens the radio device
	dev, err := rtl.Open(cfg.Index)
	if err != nil {
		return nil, err
	}

	// Creates object
	r := new(Rtlsdr)
	r.cfg = *cfg
	r.dev = dev

	// Sets default parameters
	err = rtl.SetSampleRate(r.dev, 2400000)
	if err != nil {
		return nil, err
	}

	// Create inputs
	r.inpFrequency = core.NewInputUINT(r)
	r.inpSampleRate = core.NewInputUINT(r)
	r.done = make(chan struct{})
	return r, nil
}

// Input returns the input with the specified name
func (r *Rtlsdr) Input(name string) interface{} {

	switch name {
	case "frequency":
		return r.inpFrequency
	case "samplerate":
		return r.inpSampleRate
	default:
		return nil
	}
}

// Connect connects the output of the radio to the specified input
func (r *Rtlsdr) Connect(inp interface{}) {

	r.outs = append(r.outs, inp.(*core.InputVC64))
}

// GetCaps returns this radio capabilities
func (r *Rtlsdr) GetCaps() *Caps {

	caps := Caps{
		Bands: []FreqBand{
			{24000000, 1766000000},
		},
		Samplerates: []uint32{
			3200000,
			2800000,
			2400000,
		},
	}
	return &caps
}

// Start starts the radio reception
func (r *Rtlsdr) Start() error {

	if r.started {
		log.Debug("Already started")
		return nil
	}

	// Needs to reset buffer before calling ReadAsync
	err := rtl.ResetBuffer(r.dev)
	if err != nil {
		return err
	}

	// Calls the rtlsdr ReadAsync() function in a goroutine because
	// it blocks reading data from the radio and sending to the specified callback.
	// ReadAsync() returns when CancelAsync() is called or an error occurs.
	bufCount := int(r.cfg.Bufcount)
	if bufCount == 0 {
		bufCount = 8
	}
	bufSize := int(r.cfg.Bufsize)
	if bufSize == 0 {
		bufSize = 16 * 32 * 512
	}
	log.Debug("bufCount:%v bufSize:%+v", bufCount, bufSize)
	r.started = true
	go func() {
		err := rtl.ReadAsync(r.dev, bufCount, bufSize, func(data []byte) {
			r.convRx(data)
		})
		log.Debug("ReadAsync end:%v", err)
		r.started = false
	}()

	// Starts goroutine for reading inputs
	go r.readInputs()
	return nil
}

// Stop stops the radio reception
func (r *Rtlsdr) Stop() error {

	log.Debug("STOP RX --------------------------------")
	if !r.started {
		log.Debug("Already stopped")
		return nil
	}
	// Stop goroutine for reading inputs
	r.done <- struct{}{}
	// Cancel radio read async
	return rtl.CancelAsync(r.dev)
}

// Close closes the radio device
func (r *Rtlsdr) Close() error {

	r.Stop()
	err := rtl.Close(r.dev)
	r.dev = nil
	return err
}

// Status returns the running status of this radio
func (r *Rtlsdr) Status() uint32 {

	if r.dev == nil {
		return core.Closed
	}
	if r.started {
		return core.Started
	}
	return core.Stopped
}

// Run satisfies the INode interface
func (r *Rtlsdr) Run() {

}

// convRx converts the received data from I/Q bytes to complex64 format:
// +--------+--------+                   +------------+
// | I byte | Q byte |  (2 bytes) ---->  | Complex 64 | (8 bytes)
// +--------+--------+                   +------------+
func (r *Rtlsdr) convRx(data []byte) {

	//log.Debug("Received:%d bytes: %v", len(data), data[:4])
	// Reallocates output buffers if necessary
	nsamples := len(data) / 2
	r.bufs = core.ReallocBuffersVC64(r.bufs, core.OutBuffers, nsamples)

	// Converts IQ data from byte integer to complex64
	buf := r.bufs[r.bufi]
	for i := 0; i < nsamples; i++ {
		buf[i] = complex(byte2float32(data[2*i]), byte2float32(data[2*i+1]))
	}
	//log.Debug("Received:%d samples: %v", len(buf), buf[:2])

	// Sends converted data to connected outputs
	for i := 0; i < len(r.outs); i++ {
		r.outs[i].Write(buf)
	}

	// Prepare to use next output buffer
	r.bufi++
	if r.bufi >= len(r.bufs) {
		r.bufi = 0
	}
}

// byte2float32 converts byte from 0..255 to float32 from -1..1
func byte2float32(b byte) float32 {

	i := int(b) - 127
	if i >= 0 {
		return float32(i) / 128.0
	}
	return float32(i) / 127.0
}

// readInputs runs in a goroutine when the radio is started and is
// responsible to read the source inputs and sets its values to the radio.
// Must be finished when the radio is stopped or closed by writing to done channel.
func (r *Rtlsdr) readInputs() {

loop:
	for {
		select {
		// Control channel
		case <-r.done:
			break loop
		// Frequency input channel
		case freq := <-r.inpFrequency.Chan():
			log.Debug("Setting frequency:%v", freq)
			err := rtl.SetCenterFreq(r.dev, uint32(freq))
			if err != nil {
				log.Error("Error setting frequency:%v", err)
			} else {
				log.Debug("Frequency set:%v", freq)
			}
		}
	}
	log.Debug("readInputs finished")
}
