package radio

import (
	"github.com/leonsal/godsp/core"
)

// IRadio is an interface for all radio source adapters
type IRadio interface {
	core.ISource    // Is a source
	GetCaps() *Caps // Get radio capabilities
}

// FreqBand describes the start and stop frequencies of a frequency band
type FreqBand struct {
	Start uint32
	Stop  uint32
}

// Caps describe the radio capabilities
type Caps struct {
	Bands       []FreqBand // frequency bands
	Samplerates []uint32   // sample rates
}
