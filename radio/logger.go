package radio

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("RADIO", logger.Default())
