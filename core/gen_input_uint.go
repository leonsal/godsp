package core

// File generated from template: input.t

import (
	"sync"
)

// InputUINT is an input of type uint
type InputUINT struct {
	node     INode         // associated Node
	n        uint          // input number
	ch       chan uint     // associated channel
	chWblock chan struct{} // channel to block/unblock write
	mut      sync.Mutex    // mutex to protect val
	val      uint          // set value
	set      bool          // set flag
}

// NewInputUINT creates and returns a new input of uint
// for the specified node
func NewInputUINT(node INode) *InputUINT {

	inp := new(InputUINT)
	inp.node = node
	inp.n = inp.node.inputNew()
	inp.ch = make(chan uint, InputCap)
	inp.chWblock = make(chan struct{}, 1)
	return inp
}

// Write writes the specified value to this input
// If all the nodes' inputs are ready, the node will run.
func (inp *InputUINT) Write(v uint) {

	select {
	// If can read read from write block channel, returns without writing
	case <-inp.chWblock:
		return
	// May block trying to write value to channel
	case inp.ch <- v:
		inp.node.inputWritten(inp.node, inp.n)
	}
}

// Read returns the current value of this input.
// It the input was previously set, it returns immediately with the set value.
// Otherwise it may block reading the input channel
func (inp *InputUINT) Read() uint {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel
	return <-inp.ch
}

// Peek reads the specified input without blocking
// Returns the 0 value for type if no input
func (inp *InputUINT) Peek() uint {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel without blocking
	select {
	case v := <-inp.ch:
		return v
	default:
		var v uint
		return v
	}
}

// WriteBlock should be called before restarting the node writing to this output.
func (inp *InputUINT) WriteBlock() {

	select {
	case <-inp.chWblock:
	default:
	}
}

// WriteUnblock should be used before stopping the node writing to this output.
func (inp *InputUINT) WriteUnblock() {

	select {
	case inp.chWblock <- struct{}{}:
	default:
	}
}

// Chan returns this input channel
func (inp *InputUINT) Chan() chan uint {

	return inp.ch
}

// Set sets this input with the specified value.
// A set input is always ready to read.
func (inp *InputUINT) Set(v uint) {

	inp.mut.Lock()
	inp.val = v
	inp.set = true
	inp.mut.Unlock()
	inp.node.inputSet(inp.n)
}

// Unset unsets this input
func (inp *InputUINT) Unset() {

	inp.mut.Lock()
	inp.set = false
	inp.mut.Unlock()
	inp.node.inputUnset(inp.n)
}
