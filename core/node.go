package core

import (
	"sync"
)

// INode is the interface for all node types
type INode interface {
	inputNew() uint
	inputWritten(n INode, inp uint)
	inputSet(inp uint)
	inputUnset(inp uint)
	Input(string) interface{}
	Connect(interface{})
	Run()
}

// Node implements the basic functionality of a node managing the
// number of inputs which were written or set.
type Node struct {
	mut        sync.Mutex // for exclusive access to node
	inpCount   uint       // current number of inputs
	inpMask    uint       // bitmask with all inputs
	inpWritten uint       // bitmask with inputs written
	inpSet     uint       // bitmask with inputs set
}

// inputNew is called when an input is created to set the bitmask for this input in this node.
// Returns the number of this input
func (n *Node) inputNew() uint {

	n.mut.Lock()
	ninp := n.inpCount
	n.inpMask |= (1 << ninp)
	n.inpCount++
	n.mut.Unlock()
	return ninp
}

// inputWritten indicates that the specified input was written.
// If all the node inputs are written or set the node Run()
// method will be executed.
func (n *Node) inputWritten(in INode, inp uint) {

	n.mut.Lock()
	n.inpWritten |= (1 << inp)
	if (n.inpWritten | n.inpSet) == n.inpMask {
		in.Run()
		n.inpWritten = 0
		n.mut.Unlock()
		return
	}
	n.mut.Unlock()
}

// inputSet indicates that the specified input was set.
func (n *Node) inputSet(inp uint) {

	n.mut.Lock()
	n.inpSet |= (1 << inp)
	n.mut.Unlock()
}

// inputUnset indicates that the specified input was unset.
func (n *Node) inputUnset(inp uint) {

	n.mut.Lock()
	n.inpSet &= ^(1 << inp)
	n.mut.Unlock()
}
