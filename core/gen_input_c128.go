package core

// File generated from template: input.t

import (
	"sync"
)

// InputC128 is an input of type complex128
type InputC128 struct {
	node     INode           // associated Node
	n        uint            // input number
	ch       chan complex128 // associated channel
	chWblock chan struct{}   // channel to block/unblock write
	mut      sync.Mutex      // mutex to protect val
	val      complex128      // set value
	set      bool            // set flag
}

// NewInputC128 creates and returns a new input of complex128
// for the specified node
func NewInputC128(node INode) *InputC128 {

	inp := new(InputC128)
	inp.node = node
	inp.n = inp.node.inputNew()
	inp.ch = make(chan complex128, InputCap)
	inp.chWblock = make(chan struct{}, 1)
	return inp
}

// Write writes the specified value to this input
// If all the nodes' inputs are ready, the node will run.
func (inp *InputC128) Write(v complex128) {

	select {
	// If can read read from write block channel, returns without writing
	case <-inp.chWblock:
		return
	// May block trying to write value to channel
	case inp.ch <- v:
		inp.node.inputWritten(inp.node, inp.n)
	}
}

// Read returns the current value of this input.
// It the input was previously set, it returns immediately with the set value.
// Otherwise it may block reading the input channel
func (inp *InputC128) Read() complex128 {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel
	return <-inp.ch
}

// Peek reads the specified input without blocking
// Returns the 0 value for type if no input
func (inp *InputC128) Peek() complex128 {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel without blocking
	select {
	case v := <-inp.ch:
		return v
	default:
		var v complex128
		return v
	}
}

// WriteBlock should be called before restarting the node writing to this output.
func (inp *InputC128) WriteBlock() {

	select {
	case <-inp.chWblock:
	default:
	}
}

// WriteUnblock should be used before stopping the node writing to this output.
func (inp *InputC128) WriteUnblock() {

	select {
	case inp.chWblock <- struct{}{}:
	default:
	}
}

// Chan returns this input channel
func (inp *InputC128) Chan() chan complex128 {

	return inp.ch
}

// Set sets this input with the specified value.
// A set input is always ready to read.
func (inp *InputC128) Set(v complex128) {

	inp.mut.Lock()
	inp.val = v
	inp.set = true
	inp.mut.Unlock()
	inp.node.inputSet(inp.n)
}

// Unset unsets this input
func (inp *InputC128) Unset() {

	inp.mut.Lock()
	inp.set = false
	inp.mut.Unlock()
	inp.node.inputUnset(inp.n)
}
