package core

// ISource is the interface for all sources
// A Source is node which generates signals.
type ISource interface {
	INode           // A source is also a Node
	Start() error   // Starts the source
	Stop() error    // Stops the source
	Close() error   // Close the source
	Status() uint32 // Source run status
}

// InputCap specifies the capacity of input channels.
const InputCap = 1

// OutBuffers specified the number of output buffers for sources and nodes.
// It should be at least InputCap + 2.
const OutBuffers = 3

const (
	// Stopped is the status for stopped sources.
	Stopped = 0
	// Started is the status for started sources.
	Started = 1
	// Closed is the status for closed sources.
	Closed = 2
)

// Sources manage a set sources
type Sources struct {
	sources []ISource
	status  uint32
}

// NewSources creates and returns a source manager
func NewSources() *Sources {

	ss := new(Sources)
	ss.status = Stopped
	return ss
}

// Add adds a source to this source manager
func (ss *Sources) Add(s ISource) {

	ss.sources = append(ss.sources, s)
}

// Start tries to start all the previously added sources.
func (ss *Sources) Start() error {

	for i := 0; i < len(ss.sources); i++ {
		err := ss.sources[i].Start()
		if err != nil {
			ss.Stop()
			return err
		}
	}
	ss.status = Started
	return nil
}

// Stop stops all the sources
func (ss *Sources) Stop() error {

	var lastError error
	for i := 0; i < len(ss.sources); i++ {
		err := ss.sources[i].Stop()
		if err != nil {
			lastError = err
		}
	}
	ss.status = Stopped
	return lastError
}

// Close closes all the sources
func (ss *Sources) Close() error {

	if ss.status == Closed {
		return nil
	}
	var lastError error
	for i := 0; i < len(ss.sources); i++ {
		err := ss.sources[i].Close()
		if err != nil {
			lastError = err
		}
	}
	ss.status = Closed
	return lastError
}

// Status returns the status of the sources
func (ss *Sources) Status() uint32 {

	return ss.status
}
