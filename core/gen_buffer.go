package core

// ReallocBufferVI16 reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVI16(buf []int16, length int) []int16 {

	if len(buf) >= length {
		return buf
	}
	return make([]int16, length)
}

// ReallocBuffersVI16 reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVI16(bufs [][]int16, count, length int) [][]int16 {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]int16, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]int16, length)
	}
	return bufs
}

// ReallocBufferVINT reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVINT(buf []int, length int) []int {

	if len(buf) >= length {
		return buf
	}
	return make([]int, length)
}

// ReallocBuffersVINT reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVINT(bufs [][]int, count, length int) [][]int {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]int, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]int, length)
	}
	return bufs
}

// ReallocBufferVF32 reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVF32(buf []float32, length int) []float32 {

	if len(buf) >= length {
		return buf
	}
	return make([]float32, length)
}

// ReallocBuffersVF32 reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVF32(bufs [][]float32, count, length int) [][]float32 {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]float32, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]float32, length)
	}
	return bufs
}

// ReallocBufferVF64 reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVF64(buf []float64, length int) []float64 {

	if len(buf) >= length {
		return buf
	}
	return make([]float64, length)
}

// ReallocBuffersVF64 reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVF64(bufs [][]float64, count, length int) [][]float64 {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]float64, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]float64, length)
	}
	return bufs
}

// ReallocBufferVC64 reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVC64(buf []complex64, length int) []complex64 {

	if len(buf) >= length {
		return buf
	}
	return make([]complex64, length)
}

// ReallocBuffersVC64 reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVC64(bufs [][]complex64, count, length int) [][]complex64 {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]complex64, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]complex64, length)
	}
	return bufs
}

// ReallocBufferVC128 reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferVC128(buf []complex128, length int) []complex128 {

	if len(buf) >= length {
		return buf
	}
	return make([]complex128, length)
}

// ReallocBuffersVC128 reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersVC128(bufs [][]complex128, count, length int) [][]complex128 {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]complex128, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]complex128, length)
	}
	return bufs
}
