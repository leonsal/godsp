package core

// File generated from template: input.t

import (
	"sync"
)

// InputVINT is an input of type []int
type InputVINT struct {
	node     INode         // associated Node
	n        uint          // input number
	ch       chan []int    // associated channel
	chWblock chan struct{} // channel to block/unblock write
	mut      sync.Mutex    // mutex to protect val
	val      []int         // set value
	set      bool          // set flag
}

// NewInputVINT creates and returns a new input of []int
// for the specified node
func NewInputVINT(node INode) *InputVINT {

	inp := new(InputVINT)
	inp.node = node
	inp.n = inp.node.inputNew()
	inp.ch = make(chan []int, InputCap)
	inp.chWblock = make(chan struct{}, 1)
	return inp
}

// Write writes the specified value to this input
// If all the nodes' inputs are ready, the node will run.
func (inp *InputVINT) Write(v []int) {

	select {
	// If can read read from write block channel, returns without writing
	case <-inp.chWblock:
		return
	// May block trying to write value to channel
	case inp.ch <- v:
		inp.node.inputWritten(inp.node, inp.n)
	}
}

// Read returns the current value of this input.
// It the input was previously set, it returns immediately with the set value.
// Otherwise it may block reading the input channel
func (inp *InputVINT) Read() []int {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel
	return <-inp.ch
}

// Peek reads the specified input without blocking
// Returns the 0 value for type if no input
func (inp *InputVINT) Peek() []int {

	// Try to read stick value
	inp.mut.Lock()
	if inp.set {
		val := inp.val
		inp.mut.Unlock()
		return val
	}
	inp.mut.Unlock()

	// Read value from channel without blocking
	select {
	case v := <-inp.ch:
		return v
	default:
		var v []int
		return v
	}
}

// WriteBlock should be called before restarting the node writing to this output.
func (inp *InputVINT) WriteBlock() {

	select {
	case <-inp.chWblock:
	default:
	}
}

// WriteUnblock should be used before stopping the node writing to this output.
func (inp *InputVINT) WriteUnblock() {

	select {
	case inp.chWblock <- struct{}{}:
	default:
	}
}

// Chan returns this input channel
func (inp *InputVINT) Chan() chan []int {

	return inp.ch
}

// Set sets this input with the specified value.
// A set input is always ready to read.
func (inp *InputVINT) Set(v []int) {

	inp.mut.Lock()
	inp.val = v
	inp.set = true
	inp.mut.Unlock()
	inp.node.inputSet(inp.n)
}

// Unset unsets this input
func (inp *InputVINT) Unset() {

	inp.mut.Lock()
	inp.set = false
	inp.mut.Unlock()
	inp.node.inputUnset(inp.n)
}
