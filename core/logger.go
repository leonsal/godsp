package core

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("CORE", logger.Default())
