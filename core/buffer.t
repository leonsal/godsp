package core

{{range .}}
// ReallocBufferV{{.TName}} reallocs the specified buffer for the specified minimum length if necessary.
func ReallocBufferV{{.TName}}(buf []{{.TType}}, length int) []{{.TType}} {

	if len(buf) >= length {
		return buf
	}
	return make([]{{.TType}}, length)
}

// ReallocBuffersV{{.TName}} reallocs the specified array of buffers for the specified minimum count and length
func ReallocBuffersV{{.TName}}(bufs [][]{{.TType}}, count, length int) [][]{{.TType}} {

	if len(bufs) >= count && len(bufs[0]) >= length {
		return bufs
	}

	bufs = make([][]{{.TType}}, count)
	for i := 0; i < count; i++ {
		bufs[i] = make([]{{.TType}}, length)
	}
	return bufs
}
{{end}}


