package core

//go:generate goxtempl -in=input.t -out=gen_input_i16.go     TName=I16    TType=int16
//go:generate goxtempl -in=input.t -out=gen_input_int.go     TName=INT    TType=int
//go:generate goxtempl -in=input.t -out=gen_input_uint.go    TName=UINT   TType=uint
//go:generate goxtempl -in=input.t -out=gen_input_uint32.go  TName=UINT32 TType=uint32
//go:generate goxtempl -in=input.t -out=gen_input_f32.go     TName=F32    TType=float32
//go:generate goxtempl -in=input.t -out=gen_input_f64.go     TName=F64    TType=float64
//go:generate goxtempl -in=input.t -out=gen_input_c64.go     TName=C64    TType=complex64
//go:generate goxtempl -in=input.t -out=gen_input_c128.go    TName=C128   TType=complex128

//go:generate goxtempl -in=input.t -out=gen_input_vi16.go    TName=VI16    TType=[]int16
//go:generate goxtempl -in=input.t -out=gen_input_vint.go    TName=VINT    TType=[]int
//go:generate goxtempl -in=input.t -out=gen_input_vf32.go    TName=VF32    TType=[]float32
//go:generate goxtempl -in=input.t -out=gen_input_vf64.go    TName=VF64    TType=[]float64
//go:generate goxtempl -in=input.t -out=gen_input_vc64.go    TName=VC64    TType=[]complex64
//go:generate goxtempl -in=input.t -out=gen_input_vc128.go   TName=VC128   TType=[]complex128

//go:generate goxtempl -in=buffer.t -out=gen_buffer.go TName=I16,INT,F32,F64,C64,C128 TType=int16,int,float32,float64,complex64,complex128
