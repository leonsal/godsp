package audio

//go:generate goxtempl -in=alsink.t -out=gen_alsink_vi16.go TName=I16 TType=int16   TConv=
//go:generate goxtempl -in=alsink.t -out=gen_alsink_vf32.go TName=F32 TType=float32 TConv=true
//go:generate goxtempl -in=alsink.t -out=gen_alsink_vf64.go TName=F64 TType=float64 TConv=true
