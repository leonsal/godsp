package audio

import (
	"time"
	"unsafe"

	"github.com/g3n/engine/audio/al"
	"github.com/leonsal/godsp/core"
)

// AlSinkVI16 implements an audio sink using the OpenAL library
// for input of vectors of int16.
type AlSinkVI16 struct {
	core.Node                     // Embedded node
	inpData       *core.InputVI16 // Audio data input
	inpSamplerate *core.InputUINT // Sample rate input
	inpGain       *core.InputF32  // Audio gain input
	qsize         int             // buffer queue size
	source        uint32          // OpenAL source name
	buffers       []uint32        // array of OpenAL buffer names
	nextbufi      int             // Index of next buffer to fill
	conv          []int16         // conversion buffer
	prevGain      float32         // previous gain
	timer         *time.Timer     // timer for sleeping waiting for free buffer
}

// NewAlSinkVI16 creates and returns a new audio sink
// for input of vectors of int16 with the specified buffer queue size
// (minimum is 2)
func NewAlSinkVI16(qsize int) *AlSinkVI16 {

	if qsize < 2 {
		panic("Queue size less than minimum")
	}

	s := new(AlSinkVI16)
	s.qsize = qsize
	s.inpData = core.NewInputVI16(s)
	s.inpSamplerate = core.NewInputUINT(s)
	s.inpGain = core.NewInputF32(s)
	s.inpGain.Set(1)

	// Generate buffers names
	s.buffers = al.GenBuffers(uint32(s.qsize))

	// Generate source name
	s.source = al.GenSource()

	s.prevGain = -1
	// Creates timer for sleeping
	s.timer = time.NewTimer(0)
	<-s.timer.C
	return s
}

// Input returns the input with the specified name
func (s *AlSinkVI16) Input(name string) interface{} {

	switch name {
	case "data":
		return s.inpData
	case "samplerate":
		return s.inpSamplerate
	case "gain":
		return s.inpGain
	default:
		return nil
	}
	return nil
}

// Connect satisfies the INode interface and does nothing in this case.
func (s *AlSinkVI16) Connect(inp interface{}) {

}

// Run checks if all inputs are valid and if true tries to enqueue
// the received buffer with audio data into OpenAL.
// If no space available, the function blocks waiting for OpenAL
// to play the queued buffers.
func (s *AlSinkVI16) Run() {

	// Read inputs
	data := s.inpData.Read()
	sampleRate := s.inpSamplerate.Read()
	gain := s.inpGain.Read()
	if gain != s.prevGain {
		al.Sourcef(s.source, al.Gain, gain)
		s.prevGain = gain
	}

	// Convert input to int16 if necessary
	conv := s.conv2int16(data)

	// Checks for player initial state
	state := al.GetSourcei(s.source, al.SourceState)
	var processed int32
	if state == al.Initial {

		// Queue received buffer with OpenAL
		s.queueBuffer(conv, sampleRate)

		// Starts playing
		al.SourcePlay(s.source)

		//log.Debug("AlSinkI16 Initial state")
		return
	}

	// Checks if player was stopped
	if state == al.Stopped {
		//log.Debug("AlSink STOPPED")

		// Remove processed buffers from the queue
		processed = al.GetSourcei(s.source, al.BuffersProcessed)
		//log.Debug("AlSinkI16 Remove processed: %v", processed)
		if processed > 0 {
			al.SourceUnqueueBuffers(s.source, uint32(processed), nil)
		}

		// Queue received buffer with OpenAL
		s.queueBuffer(conv, sampleRate)

		// Starts playing
		al.SourcePlay(s.source)
		return
	}

	// Checks if it is possible to enqueue this buffer
	queued := al.GetSourcei(s.source, al.BuffersQueued)
	if queued < int32(s.qsize) {
		s.queueBuffer(conv, sampleRate)
		return
	}

	// Waits for buffer processing
	for {
		processed = al.GetSourcei(s.source, al.BuffersProcessed)
		if processed > 0 {
			break
		}
		s.timer.Reset(1 * time.Millisecond)
		<-s.timer.C
	}
	//queued = al.GetSourcei(s.source, al.BuffersQueued)
	//log.Debug("AlSinkI16 processed:%v queued:%v", processed, queued)

	// Remove processed buffers from the queue
	al.SourceUnqueueBuffers(s.source, uint32(processed), nil)

	// Queue received buffer with OpenAL
	s.queueBuffer(conv, sampleRate)
}

// Stop stops the player
func (s *AlSinkVI16) Stop() {

	al.SourceStop(s.source)
}

// Dispose stops the player and dispose of the allocated resources
func (s *AlSinkVI16) Dispose() {

	al.SourceStop(s.source)
	al.DeleteSource(s.source)
	al.DeleteBuffers(s.buffers)
	//log.Debug("AlSinkI16 disposed")
}

// queueBuffer queues the specified buffer with audio data into OpenAL for playing.
func (s *AlSinkVI16) queueBuffer(conv []int16, sampleRate uint) {

	buf := s.buffers[s.nextbufi]
	al.BufferData(buf, al.FormatMono16, unsafe.Pointer(&conv[0]), uint32(2*len(conv)), uint32(sampleRate))
	al.SourceQueueBuffers(s.source, buf)
	s.nextbufi++
	if s.nextbufi >= len(s.buffers) {
		s.nextbufi = 0
	}
}

// conv2int16 convert input data in floating point format to int16
func (s *AlSinkVI16) conv2int16(data []int16) []int16 {

	return data

}
