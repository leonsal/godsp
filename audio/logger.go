package audio

import (
	"github.com/leonsal/godsp/util/logger"
)

// Package logger
var log = logger.New("AUDIO", logger.Default())
