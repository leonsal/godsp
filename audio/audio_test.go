package audio

import (
	"fmt"
	"testing"
	"time"

	"github.com/g3n/engine/audio/al"
	"github.com/leonsal/godsp/wavegen"
)

var audioOK = false

func TestAudioI16(t *testing.T) {

	fmt.Println("AlSinkVI16")
	loadAudioLibs()

	// Creates signal generator
	s := wavegen.NewSignalVI16(4 * 1024)
	s.Samplerate.Set(48000)
	s.Amplitude.Set(30000)
	s.Waveform.Set(wavegen.Sine)
	s.Offset.Set(0)

	// Creates audio sink and connect its input to signal generator
	a := NewAlSinkVI16(2)
	a.Samplerate.Set(48000)
	s.Connect(a.Input)

	// Play and stop audio
	freq := uint(440)
	for i := 0; i < 5; i++ {
		s.Frequency.Set(freq)
		s.Start()
		time.Sleep(1000 * time.Millisecond)
		s.Stop()
		time.Sleep(500 * time.Millisecond)
		freq = uint(1.059 * float64(freq))
	}
}

func TestAudioF32(t *testing.T) {

	fmt.Println("AlSinkVF32")
	loadAudioLibs()

	// Creates signal generator
	s := wavegen.NewSignalVF32(4 * 1024)
	s.Samplerate.Set(48000)
	s.Amplitude.Set(1)
	s.Waveform.Set(wavegen.Sine)
	s.Offset.Set(0)

	// Creates audio sink and connect its input to signal generator
	a := NewAlSinkVF32(2)
	a.Samplerate.Set(48000)
	s.Connect(a.Input)

	// Play and stop audio
	freq := uint(440)
	for i := 0; i < 5; i++ {
		s.Frequency.Set(freq)
		s.Start()
		time.Sleep(1000 * time.Millisecond)
		s.Stop()
		time.Sleep(500 * time.Millisecond)
		freq = uint(1.059 * float64(freq))
	}
}

func TestAudioF64(t *testing.T) {

	fmt.Println("AlSinkVF64")
	loadAudioLibs()

	// Creates signal generator
	s := wavegen.NewSignalVF64(4 * 1024)
	s.Samplerate.Set(48000)
	s.Amplitude.Set(1)
	s.Waveform.Set(wavegen.Sine)
	s.Offset.Set(0)

	// Creates audio sink and connect its input to signal generator
	a := NewAlSinkVF64(2)
	a.Samplerate.Set(48000)
	s.Connect(a.Input)

	// Play and stop audio
	freq := uint(440)
	for i := 0; i < 5; i++ {
		s.Frequency.Set(freq)
		s.Start()
		time.Sleep(1000 * time.Millisecond)
		s.Stop()
		time.Sleep(500 * time.Millisecond)
		freq = uint(1.059 * float64(freq))
	}
}

// loadAudioLibs try to load audio libraries
func loadAudioLibs() {

	if audioOK {
		return
	}

	// Try to load OpenAL
	err := al.Load()
	if err != nil {
		panic(err)
	}

	// Opens default audio device
	dev, err := al.OpenDevice("")
	if dev == nil {
		panic(fmt.Sprintf("Error: %s opening OpenAL default device", err))
	}

	// Creates audio context with auxiliary sends
	acx, err := al.CreateContext(dev, nil)
	if err != nil {
		panic(fmt.Sprintf("Error creating audio context:%s", err))
	}

	// Makes the context the current one
	err = al.MakeContextCurrent(acx)
	if err != nil {
		panic(fmt.Sprintf("Error setting audio context current:%s", err))
	}
	audioOK = true
}
