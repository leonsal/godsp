package audio

import (
	"github.com/leonsal/godsp/core"
	"github.com/g3n/engine/audio"
)


{{range .}}
// FileSourceV{{.TName}} implements an audio file source 
// which generates outputs of vector of {{.TType}}
type FileSourceV{{.TName}} struct {
	core.Source                             // embedded source
	af          *audio.AudioFile            // pointer to media audio file
	framesize   int                         // current frame size
	outs        []*core.InputV{{.TName}}    // array of outputs
	bufs        [][]{{.TType}}              // output buffers
	bufi        int                         // current output buffer index
    err         error                       // error
}


// NewFileSourceV{{.TName}} creates and returns a pointer to a new file source
// for the specified file.
func NewFileSourceV{{.TName}} (fpath string, framesize int) (*FileSourceV{{.TName}}, error) {

	// Try to open audio file
	af, err := audio.NewAudioFile(fpath)
	if err != nil {
		return nil, err
	}

	fs := new(FileSourceV{{.TName}})
	fs.Source.Init()
    fs.af = af
    fs.framesize = framesize
	//fs.bufs = core.NewBuffersV{{.TName}}(core.OutBuffers, fs.framesize)
    return fs, nil
}

// Connect connects the specified input to this source output
func (fs *FileSourceV{{.TName}}) Connect(inp *core.InputV{{.TName}}) {

	fs.outs = append(fs.outs, inp)
}


// run is a goroutine started by Start() which generates
// waveform sample frames and sends to the connected inputs
func (fs *FileSourceV{{.TName}}) run() {

	for {
		// If source closed, breaks
		if fs.Closed() {
			break
		}

		// Reads decoded audio data from file
        buf := fs.bufs[fs.bufi]
    	//n, err := fs.af.Read(&buf[0], fs.framesize)
	    //if err != nil {
        //    fs.err = err
	    //    log.Error("FileSourceV{{.TName}}:%s", err)
        //    break
	    //}

		// Sends generated data to connected inputs
		// May block if previous data not read from destination input
		for i := 0; i < len(fs.outs); i++ {
			fs.outs[i].Write(buf)
		}

		// Prepare to use next output buffer
		fs.bufi++
		if fs.bufi >= len(fs.bufs) {
			fs.bufi = 0
		}
	}
	// Closes connected inputs
	for i := 0; i < len(fs.outs); i++ {
		fs.outs[i].Close()
	}
    fs.af.Close()
	log.Debug("FileSourceV{{.TName}} finished")
}

{{end}}

